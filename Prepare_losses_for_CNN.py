import numpy as n
import cPickle as pickle         
import shelve
import sys
bins=[10,10,20] #Adapt these for different CNN
len_data=350000 #Adapt for different data size
output_neurons=50 #Adapt for different binning

with open("/data/user/mbrenzke/NN/JungleParty/branches/martins_tests/python/neural_networks/data_set_"+str(len_data)                                     +"_masking.pickle",'r') as handle:
             data=n.load(handle,'r')

features=data["features"]
target=data["target"]


max_x=features[:, :, 0].max()
min_x=features[:, :, 0].min()

max_y=features[:, :, 1].max()
min_y=features[:, :, 1].min()

max_z=features[:, :, 2].max()
min_z=features[:, :, 2].min()

edges_x=n.linspace(min_x,max_x,bins[0])
edges_y=n.linspace(min_y,max_y,bins[1])
edges_z=n.linspace(min_z,max_z,bins[2])

width_x=edges_x[1]-edges_x[0]
width_y=edges_y[1]-edges_y[0]
width_z=edges_z[1]-edges_z[0]

'''
handle=shelve.open("Data_for_CNN_grid_"+str(bins[0])+"_"+str(bins[1])+"_"+str(bins[2]),'c')
handle["features"]=[]
handle["target"]=[]
'''


        
dummy_events=n.zeros(shape=(len(features),bins[0],bins[1],bins[2]))
for w in xrange(len(features)):
        f=features[w]
        indices_x=n.digitize(f[:,0],edges_x)-1
        indices_y=n.digitize(f[:,1],edges_y)-1
        indices_z=n.digitize(f[:,2],edges_z)-1
       
        for xa in xrange(len(f[:,0])):
            if ( (indices_x[xa]!=len(edges_x)-1) and (f[:,0][xa]+width_x/2. > edges_x[indices_x[xa]+1]) ):
                indices_x[xa]+=1
        for ya in xrange(len(f[:,1])):
            if ( (indices_y[ya]!=len(edges_y)-1) and (f[:,1][ya]+width_y/2. > edges_y[indices_y[ya]+1]) ):
                indices_y[ya]+=1
        for za in xrange(len(f[:,2])):
            if ( (indices_z[za]!=len(edges_z)-1) and (f[:,2][za]+width_z/2. > edges_z[indices_z[za]+1]) ):
                indices_z[za]+=1



        '''
        for u in xrange(len(indices_x)):
            if ( ((f[u,0]-width_x/2.)>edges_x[indices_x[u]]) and (f[u,0]<=edges_x.max())):
                indices_x[u]+=1

        for m in xrange(len(indices_y)):
            if ( ((f[m,1]-width_y/2.)>edges_y[indices_y[m]]) and (f[m,1]<=edges_y.max())):
                indices_y[m]+=1

        for b in xrange(len(indices_z)):
            if ( ((f[b,2]-width_z/2.)>edges_z[indices_z[b]]) and (f[b,2]<=edges_z.max())):
                indices_z[b]+=1
        '''


  
        new_data=n.array([indices_x,indices_y,indices_z,f[:,3]])
        '''
        for k in xrange(len(indices_x)):
            new_data.append(n.array([int(indices_x[k]),int(indices_y[k]),int(indices_z[k]),f[k,3]]))
        new_data=n.array(new_data)
        '''
     
        #dummy_event=n.zeros(shape=(bins[0],bins[1],bins[2]))
        #dummy_event[w,indices_x,indices_y,indices_z]=f[:,3][np.where()]
        for o in xrange(len(new_data[3])):
            if (new_data[3,o]!=0):
            	dummy_events[w,int(new_data[0,o]),int(new_data[1,o]),int(new_data[2,o])]+=10**(new_data[3,o])
        	
        #dummy_events.append(dummy_event)
        #handle["features"].append(dummy_event)
	#print("Completed event "+str(w))
#handle["features"]=n.array(handle["features"])
#dummy_events=n.array(dummy_events)
dummy_events[dummy_events!=0]=n.log10(dummy_events[dummy_events!=0])

dummy_events=n.ma.fix_invalid(dummy_events,fill_value=0).data

new_targets=[]	

#One-hot encoding
for i in xrange(len(target)):
        new_target=n.zeros(output_neurons)
        index=int((target[i]-2)/((7.-2.)/output_neurons))
        new_target[index]=1.
        new_targets.append(new_target)
        #handle["target"].append(new_target)
#handle["target"]=n.array(handle["target"])
new_targets=n.array(new_targets)
s0=dummy_events.shape[0]
s1=dummy_events.shape[1]
s2=dummy_events.shape[2]
s3=dummy_events.shape[3]
dummy_events=dummy_events.reshape(s0,1,s1,s2,s3)
#print(sys.getsizeof(dummy_events))
#events_targets={"features":dummy_events,"target":new_targets}

comp=n.zeros(50)
comp[20]=1.
print("targets.shape: "+str(new_targets.shape))
indices=n.where(n.all(new_targets==comp,axis=1))
print("len of sample for overfitting test: "+str(len(new_targets[indices])))

n.savez_compressed("Data_for_CNN_grid_"+str(bins[0])+"_"+str(bins[1])+"_"+str(bins[2]),dummy_events,new_targets)

#n.savez_compressed("Subsample_for_overfitting_test_for_CNN_grid_"+str(bins[0])+"_"+str(bins[1])+"_"+str(bins[2]),dummy_events[indices],new_targets[indices])

'''
with open("Features_for_CNN_grid_"+str(bins[0])+"_"+str(bins[1])+"_"+str(bins[2])+".pickle",'wb') as handle:
	pickle.dump(dummy_events,handle,protocol=2)

with open("Targets_for_CNN_grid_"+str(bins[0])+"_"+str(bins[1])+"_"+str(bins[2])+".pickle",'wb') as handle:
	pickle.dump(new_targets,handle,protocol=2)
'''
