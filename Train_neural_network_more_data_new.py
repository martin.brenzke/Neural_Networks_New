#! /bin/env python
import importlib
import keras
from keras.models import Model, Sequential
from keras.layers.core import Dense, Activation, TimeDistributedDense, Masking, Dropout
from keras.layers import Input
from keras.layers.pooling import GlobalAveragePooling1D
from keras.layers.recurrent import LSTM
from keras.layers.wrappers import TimeDistributed
from keras.layers.normalization import BatchNormalization
from keras.optimizers import Nadam, SGD
from keras.callbacks import LearningRateScheduler, EarlyStopping, Callback, ModelCheckpoint #ReduceLROnPlateau
from keras.layers.advanced_activations import ELU
from keras import backend as K
from keras.layers.core import Masking
import numpy as n
from collections import defaultdict
import argparse
from sklearn import model_selection as cv
import configparser
from numpy.random import RandomState
import cPickle as pickle
import io

print("Imports done")
print("\n")


class ReduceLROnPlateau(Callback):

    '''
    Reduce learning rate when a metric has stopped improving.
    Models often benefit from reducing the learning rate by a factor
    of 2-10 once learning stagnates. This callback monitors a
    quantity and if no improvement is seen for a 'patience' number
    of epochs, the learning rate is reduced.
    # Example
        ```python
            reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.2,
                                          patience=5, min_lr=0.001)
            model.fit(X_train, Y_train, callbacks=[reduce_lr])
        ```
    # Arguments
        monitor: quantity to be monitored.
        factor: factor by which the learning rate will
            be reduced. new_lr = lr * factor
        patience: number of epochs with no improvement
            after which learning rate will be reduced.
        verbose: int. 0: quiet, 1: update messages.
        mode: one of {auto, min, max}. In `min` mode,
            lr will be reduced when the quantity
            monitored has stopped decreasing; in `max`
            mode it will be reduced when the quantity
            monitored has stopped increasing; in `auto`
            mode, the direction is automatically inferred
            from the name of the monitored quantity.
        epsilon: threshold for measuring the new optimum,
            to only focus on significant changes.
        cooldown: number of epochs to wait before resuming
            normal operation after lr has been reduced.
        min_lr: lower bound on the learning rate.
    '''
    def __init__(self, monitor='val_loss', factor=0.1, patience=10,
                 verbose=0, mode='auto', epsilon=1e-4, cooldown=0, min_lr=0):
        super(Callback, self).__init__()

        self.monitor = monitor
        if factor >= 1.0:
            raise ValueError('ReduceLROnPlateau does not support a factor >= 1.0.')
        self.factor = factor
        self.min_lr = min_lr
        self.epsilon = epsilon
        self.patience = patience
        self.verbose = verbose
        self.cooldown = cooldown
        self.cooldown_counter = 0  # Cooldown counter.
        self.wait = 0
        self.best = 0
        self.mode = mode
        self.monitor_op = None
        self.reset()

    def reset(self):
        if self.mode not in ['auto', 'min', 'max']:
            warnings.warn('Learning Rate Plateau Reducing mode %s is unknown, '
                          'fallback to auto mode.' % (self.mode), RuntimeWarning)
            self.mode = 'auto'
        if self.mode == 'min' or (self.mode == 'auto' and 'acc' not in self.monitor):
            self.monitor_op = lambda a, b: n.less(a, b - self.epsilon)
            self.best = n.Inf
        else:
            self.monitor_op = lambda a, b: n.greater(a, b + self.epsilon)
            self.best = -n.Inf
        self.cooldown_counter = 0
        self.wait = 0
        self.lr_epsilon = self.min_lr * 1e-4

    def on_train_begin(self, logs={}):
        self.reset()

    def on_epoch_end(self, epoch, logs={}):
        logs['lr'] = K.get_value(self.model.optimizer.lr)
        current = logs.get(self.monitor)
        if current is None:
            warnings.warn('Learning Rate Plateau Reducing requires %s available!' %
                          self.monitor, RuntimeWarning)
        else:
            if self.cooldown_counter > 0:
                self.cooldown_counter -= 1
                self.wait = 0

            if self.monitor_op(current, self.best):
                self.best = current
                self.wait = 0
            elif self.cooldown_counter <= 0:
                if self.wait >= self.patience:
                    old_lr = float(K.get_value(self.model.optimizer.lr))
                    if old_lr > self.min_lr + self.lr_epsilon:
                        new_lr = old_lr * self.factor
                        new_lr = max(new_lr, self.min_lr)
                        K.set_value(self.model.optimizer.lr, new_lr)
                        if self.verbose > 0:
                            print('\nEpoch %05d: reducing learning rate to %s.' % (epoch, new_lr))
                        self.cooldown_counter = self.cooldown
                self.wait += 1




if __name__ == "__main__":
    print("Start")
    print("\n")
    parser = argparse.ArgumentParser()
    parser.add_argument("--configs", required=True, dest="configs", nargs="+",
                        help="Config File")
    parser.add_argument("-o",required=True,dest="output",
                        help="Output prefix")
    parser.add_argument("-n","--nbatches", required=False, default="100",
                        dest="nbatches",type=int)
    parser.add_argument("-r", required=False, dest="learning_rate", type=float)

    args = parser.parse_args()



    #Read all config files
    print("Reading configs")
    config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
    for cfile in args.configs:
        config.read(cfile)
    train_cfg = config["training"]
    len_data=eval(train_cfg["sample_size"])
    print("Reading data")

    with open("/data/user/mbrenzke/NN/JungleParty/branches/martins_tests/python/neural_networks/data_set_"+str(len_data)+".pickle",'r') as handle:
        data=n.load(handle,'r')

#    data = n.load("/data/user/mbrenzke/NN/JungleParty/branches/martins_tests/python/neural_networks/data_set_"+str(len_data)+".pickle","r")
    features,target = data["features"],data["target"]
    maxlen = features.shape[1]
    train_size = float(train_cfg["sample_size"]) / len(target)
    rng = RandomState(0) #This will always result on the same splits
    #Split dataset into training and testing set
    features_train,features_test, \
        target_train,target_test  = cv.train_test_split(features,target,
                                                        train_size=.99,
                                                        random_state=rng) #train_size set arbitrarily might need to be changed
    del data
    print("del data executed")

    #Read NN class from config and create object
    model_wrapper = getattr(importlib.import_module("nn_models"), config["model"]["class"])()

    """
    Load if a serialized model in given in the config,
    otherwise create new model
    """
    print("Checking for serialized model and setting learning rate")
    serialized_model = config.get("paths","serialized_model",fallback=None)
    if serialized_model:
        model_wrapper.loadModel(serialized_model)
    else:
        settings = {}
        for key, value in config.items("model_settings"):
            settings[key] = eval(value)
        model_wrapper.makeModel(**settings)

    model = model_wrapper.model

    if args.learning_rate:
        learning_rate = args.learning_rate
    else:
        learning_rate = float(train_cfg["learning_rate"])

    model.optimizer.lr.set_value(learning_rate)

    """
    Depending on the model architecture, we might need multiple
    outputs or inputs. prepareDate returns the correct structure for the
    givwen model
    """
    split=config["model"]["class"].split("_")
    comp=[split[i].lower() for i in xrange(len(split))]
    test="Split"
    if test.lower() in comp:
    	x1,x2, y = model_wrapper.prepareData(features_train, target_train)
    	#x1=x1.reshape(None,x1.shape[0],x1.shape[1])
	#x2=x2.reshape(None,x2.shape[0],x2.shape[1])
	print("SPLIT")

    else:
	print("SPLITTT")
	x,y=model_wrapper.prepareData(features_train,target_train)
    #Callbacks
    print(y)
    patience=config["training"]["patience"]
    reduce_lr = ReduceLROnPlateau(monitor="val_loss", factor=0.1, patience=10, verbose=1)
    early_stop = EarlyStopping(monitor="val_loss", patience=patience, verbose=1)
    save_best= ModelCheckpoint(args.output+"_best_model.hdf",monitor="val_loss",save_best_only=True)
    callbacks = [reduce_lr, early_stop,save_best] #if int(train_cfg["lr_scaling"])!=0 else []
    print("Setup complete")
    print("training..")
    if test.lower() in comp:
	print("SPLIT2")
	
    	hist = model.fit([x1,x2],y=y,batch_size=int(train_cfg["batch_size"]),
              nb_epoch=args.nbatches,verbose=2,validation_split=float(train_cfg["validation_split"]),
              callbacks = callbacks
              )
	
    else:
	print("SPlIt")
    	hist = model.fit(x,y=y,batch_size=int(train_cfg["batch_size"]),
              nb_epoch=args.nbatches,verbose=2,validation_split=float(train_cfg["validation_split"]),
              callbacks = callbacks
              )

 
    print "Final learning rate was: ", model.optimizer.lr.get_value()

    set_train={}
    print("Saving model")
    for k,v in config.items("training"):
    	set_train[k]=eval(v)
    with io.FileIO(args.output+"_configs.txt",'w') as file:
    	file.write(str(set_train)+"\n"+str(settings)+"\n"+str(config["model"]["class"])) 
    with open(args.output+"_hist.pickle",'wb') as handle:
    	pickle.dump(hist.history,handle,protocol=2)                    
    model_wrapper.saveModel(args.output+"_model.hdf")
