"""
Module for NN architectures
"""

import keras
from keras.models import Model, Sequential
from keras.layers.core import Dense, Activation, Masking, Dropout, Flatten  #TimeDistributedDense, Masking, Dropout, Flatten
from keras.layers import Input, merge, Layer
from keras.layers.pooling import GlobalAveragePooling1D, MaxPooling3D
from keras.layers.recurrent import LSTM
from keras.layers.wrappers import TimeDistributed
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import Conv3D, ZeroPadding3D
from keras.optimizers import Nadam, SGD, Adagrad, Adadelta
from keras.callbacks import LearningRateScheduler
from keras.layers.advanced_activations import ELU
from keras import backend as K
from keras.layers.core import Masking
import numpy as n
import argparse



class NNWrapper(object):
    """
    Abstract base class for neural networks
    """
    def __init__(self):
        self.__model = None

    def get_model(self):
        """
        Getter for model
        """
        if not self.__model:
            self.makeModel()
        return self.__model

    def set_model(self, model):
        """
        Setter for model
        """
        self.__model = model

    model = property(get_model, set_model)

    def loadModel(self, filename):
        self.model = keras.models.load_model(filename)

    def makeModel(self):
        raise NotImplementedError("Needs to be implemented for derived class")

    def prepareData(self):
        raise NotImplementedError("Needs to be implemented for derived class")

    def saveModel(self, filename):
        self.model.save(filename)

class MeanOverTime(Layer):
    def __init__(self,**kwargs):
        self.supports_masking=True
        super(MeanOverTime,self).__init__(**kwargs)
    def call(self,x,mask=None):
        return K.cast(x.sum(axis=1) / mask.sum(axis=1, keepdims=True), K.floatx() )
    def get_output_shape_for(self,input_shape):
        return(input_shape[0], input_shape[2])
    def compute_mask(self,x,mask):
        return None
    def get_config(self):
        config = {}
        base_config = super(MeanOverTime,self).get_config()
        return dict(list(base_config.items()))		




class ELURNN(NNWrapper):
    """
    Recurrent neural network using a LSTM layer and multiple dense
    hidden layers with ELU neurons
    """
    def __init__(self):
        super(ELURNN, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input')
        #mask = Masking(0)(main_input) Maybe try out masking again?
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer?
        """
        norm = BatchNormalization()(main_input)
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)

        pooling = GlobalAveragePooling1D()(lstm) #Average over time dimension

        auxiliary_loss = ELU()(Dense(1, name='aux_output')(pooling)) #Aux output after LSTM layer

        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [pooling]
        for hidden in hidden_neurons:
            d_layer = ELU()(Dense(hidden,init="glorot_normal")(hidden_layers[-1]))
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate)(hidden_layers[-1])
                hidden_layers.append(dropout)
        out = ELU()(Dense(1)(hidden_layers[-1]))

        model = Model(input=[main_input], output=[out, auxiliary_loss])
        model.compile(optimizer=Nadam(), loss='mean_squared_error',
                    loss_weights=[1., 0.4]) #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs, the model will need two targets
        """
        input_list = [features]
        output_list = [target, target]

        return input_list, output_list


class ELURNN_layerwise_batch_norm(NNWrapper):
    """
    Recurrent neural network using a LSTM layer and multiple dense
    hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(ELURNN_layerwise_batch_norm, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization()(main_input)
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        norm2 = BatchNormalization()(lstm)
        pooling = GlobalAveragePooling1D()(norm2) #Average over time dimension
        norm3 = BatchNormalization()(pooling)
        auxiliary_loss = ELU()(Dense(1, name='aux_output')(pooling)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [norm3]
        for hidden in hidden_neurons:
            d_layer = ELU()(Dense(hidden,init="glorot_normal")(hidden_layers[-1]))
            d_layer = BatchNormalization()(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate)(hidden_layers[-1])
                dropout = BatchNormalization()(dropout)
                hidden_layers.append(dropout)
        out = ELU()(Dense(1)(hidden_layers[-1]))
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out, auxiliary_loss]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Nadam(), loss='mean_squared_error',
                    loss_weights=[1., 0.4]) #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target, target]

        return input_list, output_list

class ELURNN_2_LSTM_layerwise_batch_norm(NNWrapper):
    """
    Recurrent neural network using a LSTM layer and multiple dense
    hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(ELURNN_2_LSTM_layerwise_batch_norm, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization()(main_input)
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        norm2 = BatchNormalization()(lstm)
	lstm2 = LSTM(lstm_cells,dropout_U=dropout_rate,consume_less="gpu",return_sequences=True)(norm2)
	norml = BatchNormalization()(lstm2)
        pooling = GlobalAveragePooling1D()(norml) #Average over time dimension
        norm3 = BatchNormalization()(pooling)
        auxiliary_loss = ELU()(Dense(1, name='aux_output')(pooling)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [norm3]
        for hidden in hidden_neurons:
            d_layer = ELU()(Dense(hidden,init="glorot_normal")(hidden_layers[-1]))
            d_layer = BatchNormalization()(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate)(hidden_layers[-1])
                dropout = BatchNormalization()(dropout)
                hidden_layers.append(dropout)
        out = ELU()(Dense(1)(hidden_layers[-1]))
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out, auxiliary_loss]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Nadam(), loss='mean_squared_error',
                    loss_weights=[1., 0.4]) #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target, target]

        return input_list, output_list

class Mini_Network(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Mini_Network, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization()(main_input)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
#	norm2 = BatchNormalization()(norm)
#        pooling = GlobalAveragePooling1D()(norm2) #Average over time dimension
 #       norm3 = BatchNormalization()(pooling)
        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [norm]
        for hidden in hidden_neurons:
            d_layer = ELU()(Dense(hidden,init="glorot_normal")(hidden_layers[-1]))
            d_layer = BatchNormalization()(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate)(hidden_layers[-1])
                dropout = BatchNormalization()(dropout)
                hidden_layers.append(dropout)
        out = ELU()(Dense(1)(hidden_layers[-1]))
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out, auxiliary_loss]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Nadam(), loss='mean_squared_error',
                    loss_weights=[1., 0.4]) #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target, target]

        return input_list, output_list

class Mini_Network_ADAGrad(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Mini_Network_ADAGrad, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization()(main_input)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
#	norm2 = BatchNormalization()(norm)
        pooling = Flatten()(norm) #Average over time dimension
        norm3 = BatchNormalization()(pooling)
        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [norm3]
        for hidden in hidden_neurons:
            d_layer = ELU()(Dense(hidden,init="glorot_normal")(hidden_layers[-1]))
            d_layer = BatchNormalization()(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate)(hidden_layers[-1])
                dropout = BatchNormalization()(dropout)
                hidden_layers.append(dropout)
        out = ELU()(Dense(1)(hidden_layers[-1]))
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out, auxiliary_loss]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='mean_squared_error',
                    loss_weights=[1., 0.4]) #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target, target]

        return input_list, output_list
class Split_Network_ADAGrad(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Split_Network_ADAGrad, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

        input1 = Input(shape=(e_losses_max,1), dtype='float32', name='input1') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm11 = BatchNormalization()(input1)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
	#norm2 = BatchNormalization()(norm)
        pooling1 = GlobalAveragePooling1D()(norm11) #Average over time dimension
        norm13 = BatchNormalization()(pooling1)
        auxiliary_loss1 = ELU()(Dense(1, name='aux_output1')(pooling1)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers1 =  [norm13]
        for hidden in hidden_neurons:
            d_layer1 = ELU()(Dense(hidden,init="glorot_normal")(hidden_layers1[-1]))
            d_layer1 = BatchNormalization()(d_layer1)
            hidden_layers1.append(d_layer1)
            if dropout_rate > 0:
                dropout1 = Dropout(dropout_rate)(hidden_layers1[-1])
                dropout1 = BatchNormalization()(dropout1)
                hidden_layers1.append(dropout1)




	out1 = ELU()(Dense(1)(hidden_layers1[-1]))
	batch1=BatchNormalization()(out1)
        #Batch normalization at output???????????????????????????
        '''
	model1 = Model(input=[input1], output=[out1, auxiliary_loss1]) #Network yields mapped energies and aux loss
        model1.compile(optimizer=Adagrad(), loss='mean_squared_error',
                   loss_weights=[1., 0.4]) #Maybe try RMSProp?


	'''

	input2 = Input(shape=(e_losses_max,3), dtype='float32', name='input2') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm21 = BatchNormalization()(input2)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
	#norm2 = BatchNormalization()(norm)
        pooling2 = GlobalAveragePooling1D()(norm21) #Average over time dimension
        norm23 = BatchNormalization()(pooling2)
        auxiliary_loss2 = ELU()(Dense(1, name='aux_output2')(pooling2)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers2 =  [norm23]
        for hidden in hidden_neurons:
            d_layer2 = ELU()(Dense(hidden,init="glorot_normal")(hidden_layers2[-1]))
            d_layer2 = BatchNormalization()(d_layer2)
            hidden_layers2.append(d_layer2)
            if dropout_rate > 0:
                dropout2 = Dropout(dropout_rate)(hidden_layers2[-1])
                dropout2 = BatchNormalization()(dropout2)
                hidden_layers2.append(dropout2)


	out2 = ELU()(Dense(1)(hidden_layers2[-1]))
	batch2=BatchNormalization()(out2)
        #Batch normalization at output???????????????????????????
        '''
	model2 = Model(input=[input2], output=[out2,auxiliary_loss2]) #Network yields mapped energies and aux loss
        model2.compile(optimizer=Adagrad(), loss='mean_squared_error',
                    loss_weights=[1., 0.4]) #Maybe try RMSProp?
	'''

	
	merge_layer=merge([batch1,batch2],mode='concat')
        out = ELU()(Dense(1)(merge_layer))
        #Batch normalization at output???????????????????????????
        model_merge = Model(input=[input1,input2], output=[out, auxiliary_loss1,auxiliary_loss2]) #Network yields mapped energies and aux loss
        model_merge.compile(optimizer=Adagrad(), loss='mean_squared_error',
                    loss_weights=[1., 0.4,0.4]) #Maybe try RMSProp?

        self.model = model_merge

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
	
	energies=features[:,:,0]
#	print(energies.shape)
	locs=features[:,:,1:]
	energies=energies.reshape(energies.shape[0],energies.shape[1],1)
	locs=locs.reshape(locs.shape[0],locs.shape[1],locs.shape[2])
#	print(locs.shape)
#	print(energies[0])
#	input_list =n.array([energies,locs])
#	print(input_list.shape)
        output_list = [target,target,target]

        return energies, locs, output_list
class Split_Network_ADAGrad_Refeed(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Split_Network_ADAGrad_Refeed, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

        input1 = Input(shape=(e_losses_max,1), dtype='float32', name='input1') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm11 = BatchNormalization()(input1)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
	#norm2 = BatchNormalization()(norm)
        pooling1 = GlobalAveragePooling1D()(norm11) #Average over time dimension
        norm13 = BatchNormalization()(pooling1)
        auxiliary_loss1 = ELU()(Dense(1, name='aux_output1')(pooling1)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers1 =  [norm13]
        for hidden in hidden_neurons:
            d_layer1 = ELU()(Dense(hidden,init="glorot_normal")(hidden_layers1[-1]))
            d_layer1 = BatchNormalization()(d_layer1)
            hidden_layers1.append(d_layer1)
            if dropout_rate > 0:
                dropout1 = Dropout(dropout_rate)(hidden_layers1[-1])
                dropout1 = BatchNormalization()(dropout1)
                hidden_layers1.append(dropout1)




	out1 = ELU()(Dense(1)(hidden_layers1[-1]))
	batch1=BatchNormalization()(out1)
        #Batch normalization at output???????????????????????????
        '''
	model1 = Model(input=[input1], output=[out1, auxiliary_loss1]) #Network yields mapped energies and aux loss
        model1.compile(optimizer=Adagrad(), loss='mean_squared_error',
                   loss_weights=[1., 0.4]) #Maybe try RMSProp?


	'''

	input2 = Input(shape=(e_losses_max,3), dtype='float32', name='input2') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm21 = BatchNormalization()(input2)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
	#norm2 = BatchNormalization()(norm)
        pooling2 = GlobalAveragePooling1D()(norm21) #Average over time dimension
        norm23 = BatchNormalization()(pooling2)
        auxiliary_loss2 = ELU()(Dense(1, name='aux_output2')(pooling2)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers2 =  [norm23]
        for hidden in hidden_neurons:
            d_layer2 = ELU()(Dense(hidden,init="glorot_normal")(hidden_layers2[-1]))
            d_layer2 = BatchNormalization()(d_layer2)
            hidden_layers2.append(d_layer2)
            if dropout_rate > 0:
                dropout2 = Dropout(dropout_rate)(hidden_layers2[-1])
                dropout2 = BatchNormalization()(dropout2)
                hidden_layers2.append(dropout2)


	out2 = ELU()(Dense(1)(hidden_layers2[-1]))
	batch2=BatchNormalization()(out2)
        #Batch normalization at output???????????????????????????
        '''
	model2 = Model(input=[input2], output=[out2,auxiliary_loss2]) #Network yields mapped energies and aux loss
        model2.compile(optimizer=Adagrad(), loss='mean_squared_error',
                    loss_weights=[1., 0.4]) #Maybe try RMSProp?
	'''

	
	merge_layer=merge([batch1,batch2],mode='concat')
	norm_m1=BatchNormalization()(merge_layer)
	hidden_layers3 =  [norm_m1]
        for hidden in hidden_neurons:
            d_layer3 = ELU()(Dense(hidden,init="glorot_normal")(hidden_layers3[-1]))
            d_layer3 = BatchNormalization()(d_layer3)
            hidden_layers3.append(d_layer3)
            if dropout_rate > 0:
                dropout3 = Dropout(dropout_rate)(hidden_layers3[-1])
                dropout3 = BatchNormalization()(dropout3)
                hidden_layers3.append(dropout3)


	merge_layer2=merge([hidden_layers3[-1],batch1,batch2],mode='concat')
        out = ELU()(Dense(1)(merge_layer2))
        #Batch normalization at output???????????????????????????
        model_merge = Model(input=[input1,input2], output=[out, auxiliary_loss1,auxiliary_loss2]) #Network yields mapped energies and aux loss
        model_merge.compile(optimizer=Adagrad(), loss='mean_squared_error',
                    loss_weights=[1., 0.4,0.4]) #Maybe try RMSProp?

        self.model = model_merge

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
	
	energies=features[:,:,0]
#	print(energies.shape)
	locs=features[:,:,1:]
	energies=energies.reshape(energies.shape[0],energies.shape[1],1)
	locs=locs.reshape(locs.shape[0],locs.shape[1],locs.shape[2])
#	print(locs.shape)
#	print(energies[0])
#	input_list =n.array([energies,locs])
#	print(input_list.shape)
        output_list = [target,target,target]

        return energies, locs, output_list

class One_LSTM_one_hidden_layer_ADAGrad(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(One_LSTM_one_hidden_layer_ADAGrad, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization()(main_input)
	
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        
	norm2 = BatchNormalization()(lstm)
        pooling = GlobalAveragePooling1D()(norm2) #Average over time dimension
        norm3 = BatchNormalization()(pooling)
        auxiliary_loss = ELU()(Dense(1, name='aux_output')(pooling)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [norm3]
        for hidden in hidden_neurons:
            d_layer = ELU()(Dense(hidden,init="glorot_normal")(hidden_layers[-1]))
            d_layer = BatchNormalization()(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate)(hidden_layers[-1])
                dropout = BatchNormalization()(dropout)
                hidden_layers.append(dropout)
        out = ELU()(Dense(1)(hidden_layers[-1]))
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out, auxiliary_loss]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='mean_squared_error',
                    loss_weights=[1., 0.4]) #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target, target]

        return input_list, output_list
class Split_Mini_RNN_ADAGrad(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Split_Mini_RNN_ADAGrad, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

        input1 = Input(shape=(e_losses_max,1), dtype='float32', name='input1') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm11 = BatchNormalization()(input1)
	
        lstm1 = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm11)
        
	#norm2 = BatchNormalization()(norm)
        pooling1 = GlobalAveragePooling1D()(lstm1) #Average over time dimension
        norm13 = BatchNormalization()(pooling1)
        auxiliary_loss1 = ELU()(Dense(1, name='aux_output1')(pooling1)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers1 =  [norm13]
        for hidden in hidden_neurons:
            d_layer1 = ELU()(Dense(hidden,init="glorot_normal")(hidden_layers1[-1]))
            d_layer1 = BatchNormalization()(d_layer1)
            hidden_layers1.append(d_layer1)
            if dropout_rate > 0:
                dropout1 = Dropout(dropout_rate)(hidden_layers1[-1])
                dropout1 = BatchNormalization()(dropout1)
                hidden_layers1.append(dropout1)




	out1 = ELU()(Dense(1)(hidden_layers1[-1]))
	batch1=BatchNormalization()(out1)
        #Batch normalization at output???????????????????????????
        '''
	model1 = Model(input=[input1], output=[out1, auxiliary_loss1]) #Network yields mapped energies and aux loss
        model1.compile(optimizer=Adagrad(), loss='mean_squared_error',
                   loss_weights=[1., 0.4]) #Maybe try RMSProp?


	'''

	input2 = Input(shape=(e_losses_max,3), dtype='float32', name='input2') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm21 = BatchNormalization()(input2)
	
        lstm2 = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm21)
        
	#norm2 = BatchNormalization()(norm)
        pooling2 = GlobalAveragePooling1D()(lstm2) #Average over time dimension
        norm23 = BatchNormalization()(pooling2)
        auxiliary_loss2 = ELU()(Dense(1, name='aux_output2')(pooling2)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers2 =  [norm23]
        for hidden in hidden_neurons:
            d_layer2 = ELU()(Dense(hidden,init="glorot_normal")(hidden_layers2[-1]))
            d_layer2 = BatchNormalization()(d_layer2)
            hidden_layers2.append(d_layer2)
            if dropout_rate > 0:
                dropout2 = Dropout(dropout_rate)(hidden_layers2[-1])
                dropout2 = BatchNormalization()(dropout2)
                hidden_layers2.append(dropout2)


	out2 = ELU()(Dense(1)(hidden_layers2[-1]))
	batch2=BatchNormalization()(out2)
        #Batch normalization at output???????????????????????????
        '''
	model2 = Model(input=[input2], output=[out2,auxiliary_loss2]) #Network yields mapped energies and aux loss
        model2.compile(optimizer=Adagrad(), loss='mean_squared_error',
                    loss_weights=[1., 0.4]) #Maybe try RMSProp?
	'''

	
	merge_layer=merge([batch1,batch2],mode='concat')
        out = ELU()(Dense(1)(merge_layer))
        #Batch normalization at output???????????????????????????
        model_merge = Model(input=[input1,input2], output=[out, auxiliary_loss1,auxiliary_loss2]) #Network yields mapped energies and aux loss
        model_merge.compile(optimizer=Adagrad(), loss='mean_squared_error',
                    loss_weights=[1., 0.4,0.4]) #Maybe try RMSProp?

        self.model = model_merge

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
	
	energies=features[:,:,0]
#	print(energies.shape)
	locs=features[:,:,1:]
	energies=energies.reshape(energies.shape[0],energies.shape[1],1)
	locs=locs.reshape(locs.shape[0],locs.shape[1],locs.shape[2])
#	print(locs.shape)
#	print(energies[0])
#	input_list =n.array([energies,locs])
#	print(input_list.shape)
        output_list = [target,target,target]

        return energies, locs, output_list
class Mini_RNN_RELU_LSTM_ADAGrad(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Mini_RNN_RELU_LSTM_ADAGrad, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization()(main_input)
	
        lstm = LSTM(lstm_cells,
		    activation='relu',
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        
	norm2 = BatchNormalization()(lstm)
        pooling = GlobalAveragePooling1D()(norm2) #Average over time dimension
        norm3 = BatchNormalization()(pooling)
        auxiliary_loss = ELU()(Dense(1, name='aux_output')(pooling)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [norm3]
        for hidden in hidden_neurons:
            d_layer = ELU()(Dense(hidden,init="glorot_normal")(hidden_layers[-1]))
            d_layer = BatchNormalization()(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate)(hidden_layers[-1])
                dropout = BatchNormalization()(dropout)
                hidden_layers.append(dropout)
        out = ELU()(Dense(1)(hidden_layers[-1]))
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out, auxiliary_loss]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='mean_squared_error',
                    loss_weights=[1., 0.4]) #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target, target]

        return input_list, output_list

class Mini_Network_only_energy_losses(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Mini_Network_only_energy_losses, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

        main_input = Input(shape=(e_losses_max,1), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization()(main_input)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
#	norm2 = BatchNormalization()(norm)
        pooling = GlobalAveragePooling1D()(norm) #Average over time dimension
        norm3 = BatchNormalization()(pooling)
        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [norm3]
        for hidden in hidden_neurons:
            d_layer = ELU()(Dense(hidden,init="glorot_normal")(hidden_layers[-1]))
            d_layer = BatchNormalization()(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate)(hidden_layers[-1])
                dropout = BatchNormalization()(dropout)
                hidden_layers.append(dropout)
        out = ELU()(Dense(1)(hidden_layers[-1]))
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out, auxiliary_loss]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='mean_squared_error',
                    loss_weights=[1., 0.4]) #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = features[:,:,0]
        input_list=input_list.reshape(input_list.shape[0],input_list.shape[1],1) 
        output_list = [target, target]

        return input_list, output_list

class Test_Mini_Network_ADAGrad(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Mini_Network_ADAGrad, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization(name='norm')(main_input)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
#	norm2 = BatchNormalization()(norm)
        pooling = Flatten(name='pooling')(norm) #Average over time dimension
#        norm3 = BatchNormalization()(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [pooling]
        count=1
        for hidden in hidden_neurons:
            d_layer = ELU()(Dense(hidden,init="glorot_normal",name='hidden'+str(count))(hidden_layers[-1]))
            d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
                dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out = ELU()(Dense(1,init="glorot_normal",name='out')(hidden_layers[-1]))
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='mean_squared_error') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target]

        return input_list, output_list

class Test_Mini_Network_ADAGrad_Sum_Activation(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Mini_Network_ADAGrad_Sum_Activation, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

	

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization(name='norm')(main_input)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
#	norm2 = BatchNormalization()(norm)
        pooling = Flatten(name='pooling')(norm) #Average over time dimension
#        norm3 = BatchNormalization()(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [pooling]
        count=1
        for hidden in hidden_neurons:
            d_layer = ELU()(Dense(hidden,init="one",name='hidden'+str(count))(hidden_layers[-1]))
            d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
                dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out = Dense(1,init="one",name='out')(hidden_layers[-1])
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='mean_squared_error') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target]

        return input_list, output_list


class Test_Mini_Network_ADAGrad_No_Activation(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Mini_Network_ADAGrad_No_Activation, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

	

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization(name='norm')(main_input)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
#	norm2 = BatchNormalization()(norm)
        pooling = Flatten(name='pooling')(norm) #Average over time dimension
#        norm3 = BatchNormalization()(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [pooling]
        count=1
        for hidden in hidden_neurons:
            d_layer = Dense(hidden,init="one",name='hidden'+str(count))(hidden_layers[-1])
            d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
                dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out = Dense(1,init="one",name='out')(hidden_layers[-1])
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='mean_squared_error') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target]

        return input_list, output_list


#########Implement masking layer, maybe only works for RNN  ###########################################

class Test_Mini_Network_ADAGrad_Averaging_No_Activation_mask_zeros(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Mini_Network_ADAGrad_Averaging_No_Activation_mask_zeros, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

	

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        mask = Masking(mask_value=0,name="mask")(main_input)  # Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization(name='norm')(mask)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
#	norm2 = BatchNormalization()(norm)
        pooling = MeanOverTime(name='pooling')(norm) #Average over time dimension
#        norm3 = BatchNormalization(name='norm3')(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [pooling]
        count=1
        for hidden in hidden_neurons:
            d_layer =Dense(hidden,init="glorot_normal",name='hidden'+str(count))(hidden_layers[-1])
            d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
                #dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out = Dense(1,init="glorot_normal",name='out')(hidden_layers[-1])
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='mean_squared_error') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target]

        return input_list, output_list



class One_LSTM_one_hidden_layer_ADAGrad_masking(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(One_LSTM_one_hidden_layer_ADAGrad_masking, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        mask = Masking(mask_value=0,name='mask')(main_input) #Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization()(mask)
	
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True,init='normal')(norm)
        
	norm2 = BatchNormalization()(lstm)
        pooling = MeanOverTime(name='pooling')(norm2) #Average over time dimension
        norm3 = BatchNormalization()(pooling)
        auxiliary_loss = ELU()(Dense(1, name='aux_output',init='normal')(pooling)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [norm3]
        for hidden in hidden_neurons:
            d_layer = ELU()(Dense(hidden,init="normal")(hidden_layers[-1]))
            d_layer = BatchNormalization()(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate)(hidden_layers[-1])
                dropout = BatchNormalization()(dropout)
                hidden_layers.append(dropout)
        out = ELU()(Dense(1,init='normal')(hidden_layers[-1]))
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out, auxiliary_loss]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='mean_squared_error',
                    loss_weights=[1., 0.4]) #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target, target]

        return input_list, output_list

class ELURNN_layerwise_batch_norm_ADAGrad_mask_zeros(NNWrapper):
    """
    Recurrent neural network using a LSTM layer and multiple dense
    hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(ELURNN_layerwise_batch_norm_ADAGrad_mask_zeros, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        mask = Masking(mask_value=0,name='mask')(main_input) #Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization()(mask)
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        norm2 = BatchNormalization()(lstm)
        pooling = MeanOverTime(name='pooling')(norm2) #Average over time dimension
        norm3 = BatchNormalization()(pooling)
        auxiliary_loss = ELU()(Dense(1, name='aux_output')(pooling)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [norm3]
        for hidden in hidden_neurons:
            d_layer = ELU()(Dense(hidden,init="glorot_normal")(hidden_layers[-1]))
            d_layer = BatchNormalization()(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate)(hidden_layers[-1])
                dropout = BatchNormalization()(dropout)
                hidden_layers.append(dropout)
        out = ELU()(Dense(1)(hidden_layers[-1]))
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out, auxiliary_loss]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='mean_squared_error',
                    loss_weights=[1., 0.4]) #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target, target]

        return input_list, output_list





######## Mini NN for didactical approach #############################################


class Test_Mini_Network_ADAGrad_Averaging_ELU_Activation_mask_zeros(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Mini_Network_ADAGrad_Averaging_ELU_Activation_mask_zeros, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

	

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        mask = Masking(mask_value=0,name='mask')(main_input)  # Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization(name='norm')(mask)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
#	norm2 = BatchNormalization()(norm)
        pooling = MeanOverTime(name='pooling')(norm) #Average over time dimension
#        norm3 = BatchNormalization(name='norm3')(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [pooling]
        count=1
        for hidden in hidden_neurons:
            d_layer =ELU() (Dense(hidden,init="glorot_normal",name='hidden'+str(count))(hidden_layers[-1]))
            d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
                #dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out =ELU() (Dense(1,init="glorot_normal",name='out')(hidden_layers[-1]))
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='mean_squared_error') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target]

        return input_list, output_list





class Test_Mini_Network_ADAGrad_Averaging_ELU_Activation_dont_mask_zeros(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Mini_Network_ADAGrad_Averaging_ELU_Activation_dont_mask_zeros, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

	

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        #mask = Masking(mask_value=0,name='mask')(main_input)  # Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization(name='norm')(main_input)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
#	norm2 = BatchNormalization()(norm)
        pooling = GlobalAveragePooling1D(name='pooling')(norm) #Average over time dimension
#        norm3 = BatchNormalization(name='norm3')(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [pooling]
        count=1
        for hidden in hidden_neurons:
            d_layer =ELU() (Dense(hidden,init="glorot_normal",name='hidden'+str(count))(hidden_layers[-1]))
            d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
                #dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out =ELU() (Dense(1,init="glorot_normal",name='out')(hidden_layers[-1]))
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='mean_squared_error') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target]

        return input_list, output_list


### including dropout ###########

class Test_Mini_Network_ADAGrad_Averaging_ELU_Activation_mask_zeros_dropout(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Mini_Network_ADAGrad_Averaging_ELU_Activation_mask_zeros_dropout, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

	

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        mask = Masking(mask_value=0,name='mask')(main_input)  # Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization(name='norm')(mask)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
#	norm2 = BatchNormalization()(norm)
        pooling = MeanOverTime(name='pooling')(norm) #Average over time dimension
#        norm3 = BatchNormalization(name='norm3')(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [pooling]
        count=1
        for hidden in hidden_neurons:
            d_layer =ELU() (Dense(hidden,init="glorot_normal",name='hidden'+str(count))(hidden_layers[-1]))
            d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
                #dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out =ELU() (Dense(1,init="glorot_normal",name='out')(hidden_layers[-1]))
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='mean_squared_error') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target]

        return input_list, output_list

class Test_Mini_Network_ADAGrad_Averaging_ELU_Activation_dont_mask_zeros_dropout(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Mini_Network_ADAGrad_Averaging_ELU_Activation_dont_mask_zeros_dropout, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

	

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        #mask = Masking(mask_value=0,name='mask')(main_input)  # Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization(name='norm')(main_input)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
#	norm2 = BatchNormalization()(norm)
        pooling = GlobalAveragePooling1D(name='pooling')(norm) #Average over time dimension
#        norm3 = BatchNormalization(name='norm3')(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [pooling]
        count=1
        for hidden in hidden_neurons:
            d_layer =ELU() (Dense(hidden,init="glorot_normal",name='hidden'+str(count))(hidden_layers[-1]))
            d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
                #dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out =ELU() (Dense(1,init="glorot_normal",name='out')(hidden_layers[-1]))
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='mean_squared_error') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target]

        return input_list, output_list


#### No batch_norm, with dropout#######################

class Test_Mini_Network_ADAGrad_Averaging_ELU_Activation_mask_zeros_dropout_no_batch_norm(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Mini_Network_ADAGrad_Averaging_ELU_Activation_mask_zeros_dropout_no_batch_norm, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

	

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        mask = Masking(mask_value=0,name='mask')(main_input)  # Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
       # norm = BatchNormalization(name='norm')(mask)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
#	norm2 = BatchNormalization()(norm)
        pooling = MeanOverTime(name='pooling')(mask) #Average over time dimension
#        norm3 = BatchNormalization(name='norm3')(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [pooling]
        count=1
        for hidden in hidden_neurons:
            d_layer =ELU() (Dense(hidden,init="glorot_normal",name='hidden'+str(count))(hidden_layers[-1]))
            #d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
                #dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out =ELU() (Dense(1,init="glorot_normal",name='out')(hidden_layers[-1]))
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='mean_squared_error') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target]

        return input_list, output_list

class Test_Mini_Network_ADAGrad_Averaging_ELU_Activation_dont_mask_zeros_dropout_no_batch_norm(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Mini_Network_ADAGrad_Averaging_ELU_Activation_dont_mask_zeros_dropout_no_batch_norm, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

	

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        #mask = Masking(mask_value=0,name='mask')(main_input)  # Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        #norm = BatchNormalization(name='norm')(main_input)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
#	norm2 = BatchNormalization()(norm)
        pooling = GlobalAveragePooling1D(name='pooling')(main_input) #Average over time dimension
#        norm3 = BatchNormalization(name='norm3')(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [pooling]
        count=1
        for hidden in hidden_neurons:
            d_layer =ELU() (Dense(hidden,init="glorot_normal",name='hidden'+str(count))(hidden_layers[-1]))
            #d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
                #dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out =ELU() (Dense(1,init="glorot_normal",name='out')(hidden_layers[-1]))
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='mean_squared_error') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target]

        return input_list, output_list

######### No batch norm, no dropout #################################################################################
     ##### Just use NN without batch_norm with param dropout_rate=0, new classes only used for correct naming scheme ####################
class Test_Mini_Network_ADAGrad_Averaging_ELU_Activation_mask_zeros_no_dropout_no_batch_norm(Test_Mini_Network_ADAGrad_Averaging_ELU_Activation_mask_zeros_dropout_no_batch_norm):
    pass

class Test_Mini_Network_ADAGrad_Averaging_ELU_Activation_dont_mask_zeros_no_dropout_no_batch_norm(Test_Mini_Network_ADAGrad_Averaging_ELU_Activation_dont_mask_zeros_dropout_no_batch_norm):
    pass


####### Try stuff with flatten ######################################################################################

class Test_Mini_Network_ADAGrad_Flatten_ELU_Activation_dont_mask_zeros_layerwise_batch_norm_no_dropout(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Mini_Network_ADAGrad_Flatten_ELU_Activation_dont_mask_zeros_layerwise_batch_norm_no_dropout, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

	

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization(name='norm')(main_input)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
#	norm2 = BatchNormalization()(norm)
        pooling = Flatten(name='pooling')(norm) #Average over time dimension
#        norm3 = BatchNormalization()(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [pooling]
        count=1
        for hidden in hidden_neurons:
            d_layer = ELU() (Dense(hidden,init="glorot_normal",name='hidden'+str(count))(hidden_layers[-1]))
            d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
 #               dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out = ELU() (Dense(1,init="glorot_normal",name='out')(hidden_layers[-1]))
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='mean_squared_error') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target]

        return input_list, output_list

class Test_Mini_Network_ADAGrad_Flatten_ELU_Activation_dont_mask_zeros_no_batch_norm_no_dropout(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Mini_Network_ADAGrad_Flatten_ELU_Activation_dont_mask_zeros_no_batch_norm_no_dropout, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

	

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        #norm = BatchNormalization(name='norm')(main_input)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
#	norm2 = BatchNormalization()(norm)
        pooling = Flatten(name='pooling')(main_input) #Average over time dimension
#        norm3 = BatchNormalization()(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [pooling]
        count=1
        for hidden in hidden_neurons:
            d_layer = ELU() (Dense(hidden,init="glorot_normal",name='hidden'+str(count))(hidden_layers[-1]))
            #d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
                #dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out = ELU() (Dense(1,init="glorot_normal",name='out')(hidden_layers[-1]))
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='mean_squared_error') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target]

        return input_list, output_list


class Test_Mini_Network_ADAGrad_Flatten_ELU_Activation_dont_mask_zeros_no_batch_norm_dropout(Test_Mini_Network_ADAGrad_Flatten_ELU_Activation_dont_mask_zeros_no_batch_norm_no_dropout):
    pass


class Test_Mini_Network_ADAGrad_Flatten_ELU_Activation_dont_mask_zeros_layerwise_batch_norm_dropout(Test_Mini_Network_ADAGrad_Flatten_ELU_Activation_dont_mask_zeros_layerwise_batch_norm_no_dropout):
    pass
    

class Test_Mini_Network_ADADelta_Flatten_ELU_Activation_dont_mask_zeros_no_batch_norm_dropout(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Mini_Network_ADADelta_Flatten_ELU_Activation_dont_mask_zeros_no_batch_norm_dropout, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

	

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        #norm = BatchNormalization(name='norm')(main_input)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
#	norm2 = BatchNormalization()(norm)
        pooling = Flatten(name='pooling')(main_input) #Average over time dimension
#        norm3 = BatchNormalization()(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [pooling]
        count=1
        for hidden in hidden_neurons:
            d_layer = ELU() (Dense(hidden,init="glorot_normal",name='hidden'+str(count))(hidden_layers[-1]))
            #d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
                #dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out = ELU() (Dense(1,init="glorot_normal",name='out')(hidden_layers[-1]))
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adadelta(), loss='mean_squared_error') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target]

        return input_list, output_list

class Test_Mini_Network_ADADelta_Flatten_ELU_Activation_dont_mask_zeros_layerwise_batch_norm_no_dropout(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Mini_Network_ADADelta_Flatten_ELU_Activation_dont_mask_zeros_layerwise_batch_norm_no_dropout, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

	

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization(name='norm')(main_input)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
#	norm2 = BatchNormalization()(norm)
        pooling = Flatten(name='pooling')(norm) #Average over time dimension
#        norm3 = BatchNormalization()(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [pooling]
        count=1
        for hidden in hidden_neurons:
            d_layer = ELU() (Dense(hidden,init="glorot_normal",name='hidden'+str(count))(hidden_layers[-1]))
            d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
#                dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out = ELU() (Dense(1,init="glorot_normal",name='out')(hidden_layers[-1]))
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adadelta(), loss='mean_squared_error') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target]

        return input_list, output_list




class Test_Mini_Network_ADAGrad_mean_x_y_Flatten_z_dE_ELU_Activation_layerwise_batch_norm_dropout_mask_zeros_for_mean(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Mini_Network_ADAGrad_mean_x_y_Flatten_z_dE_ELU_Activation_layerwise_batch_norm_dropout_mask_zeros_for_mean, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

	

        input_x = Input(shape=(1,), dtype='float32', name='input_x') #in_features=4 for 3D x and 1D E
        input_y = Input(shape=(1,), dtype='float32', name='input_y')
        input_zs = Input(shape=(e_losses_max,), dtype='float32', name='input_zs')
        input_dE = Input(shape=(e_losses_max,), dtype='float32', name='input_dE')
        inputs=[input_x,input_y,input_zs,input_dE]
        merge_in=merge([input_x,input_y,input_zs,input_dE], mode='concat', name='merge_in')
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization(name='norm')(merge_in)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
#	norm2 = BatchNormalization()(norm)
     #   pooling = Flatten(name='pooling')(norm) #Average over time dimension
#        norm3 = BatchNormalization()(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [norm]
        count=1
        for hidden in hidden_neurons:
            d_layer = ELU() (Dense(hidden,init="glorot_normal",name='hidden'+str(count))(hidden_layers[-1]))
            d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
               # dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out = ELU() (Dense(1,init="glorot_normal",name='out')(hidden_layers[-1]))
        #Batch normalization at output???????????????????????????
        model = Model(input=[input_x,input_y,input_zs,input_dE], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='mean_squared_error') #Maybe try RMSProp?

        self.model = model

#### Input to NN???????? #####################################

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        x_means=[]
        y_means=[]
        zs=[]
        dEs=[]
        events=[]
        for i in xrange(len(features)):
  #           print("feature: "+str(features[i]))
             count_non_zeros=0
             for j in xrange(len(features[i])):
                    if not  (n.all(features[i,j])==0):
                        count_non_zeros+=1.
             x_means.append(features[i,:,0].sum()/count_non_zeros)
             y_means.append(features[i,:,1].sum()/count_non_zeros) 
             zs.append(features[i,:,2].flatten())
             dEs.append(features[i,:,3].flatten())
            # temp_event=[features[i,:,0],features[i,:,1],features[i,:,2].flatten(),features[i,:,3].flatten()]
            # events.append(temp_event)
 #            print("mean x: "+str(x_means[i]))   
        x_means=n.array(x_means)
        y_means=n.array(y_means)
        zs=n.array(zs)
        dEs=n.array(dEs)
       # z=features[:,:,2]
       # dE=features[:,:,3]
       # new_features=[]
       # for k in xrange(len(x_means)):
       #     temp=[x_means[k],y_means[k],zs,dEs]
       #     new_features.append(temp)
        input_list = [x_means,y_means,zs,dEs]
        output_list = [target]

        return input_list, output_list

### Testing RELU #######################################
class Test_Mini_Network_ADAGrad_Flatten_RELU_Activation_dont_mask_zeros_layerwise_batch_norm_dropout(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Mini_Network_ADAGrad_Flatten_RELU_Activation_dont_mask_zeros_layerwise_batch_norm_dropout, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

	

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization(name='norm')(main_input)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
#	norm2 = BatchNormalization()(norm)
        pooling = Flatten(name='pooling')(norm) #Average over time dimension
#        norm3 = BatchNormalization()(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [pooling]
        count=1
        for hidden in hidden_neurons:
            d_layer = Dense(hidden,init="glorot_normal",name='hidden'+str(count),activation='relu')(hidden_layers[-1])
            d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
 #               dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out = Dense(1,init="glorot_normal",name='out',activation='relu')(hidden_layers[-1])
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='mean_squared_error') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target]

        return input_list, output_list

## NN ordering data by energy loss ##################


class Test_Mini_Network_ADAGrad_Flatten_ELU_Activation_dont_mask_zeros_layerwise_batch_norm_dropout_ordered_losses(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Mini_Network_ADAGrad_Flatten_ELU_Activation_dont_mask_zeros_layerwise_batch_norm_dropout_ordered_losses, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

	

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization(name='norm')(main_input)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
#	norm2 = BatchNormalization()(norm)
        pooling = Flatten(name='pooling')(norm) #Average over time dimension
#        norm3 = BatchNormalization()(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [pooling]
        count=1
        for hidden in hidden_neurons:
            d_layer = ELU() (Dense(hidden,init="glorot_normal",name='hidden'+str(count))(hidden_layers[-1]))
            d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
 #               dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out = ELU() (Dense(1,init="glorot_normal",name='out')(hidden_layers[-1]))
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='mean_squared_error') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
	for i in xrange(len(features)):
		if (i==0):
			print("feature 1: "+str(features[i]))
		features[i]=n.sort(features[i,:].T).T
		if (i==0):
			print("feature 1 sorted: "+str(features[i]))
        input_list = [features]
        output_list = [target]

        return input_list, output_list


### Towards softmax output #################
class Test_Mini_Network_ADAGrad_Flatten_ELU_Activation_dont_mask_zeros_layerwise_batch_norm_dropout_softmax(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Mini_Network_ADAGrad_Flatten_ELU_Activation_dont_mask_zeros_layerwise_batch_norm_dropout_softmax, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5,output_neurons=50) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

	

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization(name='norm')(main_input)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
#	norm2 = BatchNormalization()(norm)
        pooling = Flatten(name='pooling')(norm) #Average over time dimension
#        norm3 = BatchNormalization()(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [pooling]
        count=1
        for hidden in hidden_neurons:
            d_layer = ELU() (Dense(hidden,init="glorot_normal",name='hidden'+str(count))(hidden_layers[-1]))
            d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
 #               dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out = Dense(output_neurons,init="glorot_normal",name='out',activation='softmax')(hidden_layers[-1])
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='categorical_crossentropy') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target,output_neurons):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        bins=n.array([1.+i*(9.-1.)/output_neurons for i in xrange(output_neurons)]) #range of E_reco????
        new_targets=[]
	#One-hot encoding
        for i in target:
            new_target=n.zeros(output_neurons)
            index=int((i-1)/((9.-1.)/output_neurons))
            new_target[index]=1.
            new_targets.append(new_target)
        new_targets=n.array(new_targets)
        input_list = [features]
        output_list = [new_targets]

        return input_list, output_list
##softmax with ordered inputs#######
class Test_Mini_Network_ADAGrad_Flatten_ELU_Activation_dont_mask_zeros_layerwise_batch_norm_dropout_softmax_ordered_inputs(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Mini_Network_ADAGrad_Flatten_ELU_Activation_dont_mask_zeros_layerwise_batch_norm_dropout_softmax_ordered_inputs, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5,output_neurons=50) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

	

        main_input = Input(shape=(e_losses_max,in_features), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        norm = BatchNormalization(name='norm')(main_input)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
#	norm2 = BatchNormalization()(norm)
        pooling = Flatten(name='pooling')(norm) #Average over time dimension
#        norm3 = BatchNormalization()(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [pooling]
        count=1
        for hidden in hidden_neurons:
            d_layer = ELU() (Dense(hidden,init="glorot_normal",name='hidden'+str(count))(hidden_layers[-1]))
            d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
 #               dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out = Dense(output_neurons,init="glorot_normal",name='out',activation='softmax')(hidden_layers[-1])
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='categorical_crossentropy') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target,output_neurons):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        bins=n.array([2.+i*(7.-2.)/output_neurons for i in xrange(output_neurons)]) #range of E_reco????
        new_targets=[]	
        #sort inputs by energy loss
        for i in xrange(len(features)):
		if (i==0):
			print("feature 1: "+str(features[i]))
		features[i]=n.sort(features[i,:].T).T
		if (i==0):
			print("feature 1 sorted: "+str(features[i]))
        #One-hot encoding
        for i in target:
            new_target=n.zeros(output_neurons)
            index=int((i-2)/((7.-2.)/output_neurons))
            new_target[index]=1.
            new_targets.append(new_target)
        new_targets=n.array(new_targets)
        input_list = [features]
        output_list = [new_targets]

        return input_list, output_list


################################################################################################################################
########################### Convolutional stuff ################################################################################
################################################################################################################################

class Test_Conv_Network_ADAGrad_ELU_Activation_dont_mask_zeros_layerwise_batch_norm_dropout_softmax(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Conv_Network_ADAGrad_ELU_Activation_dont_mask_zeros_layerwise_batch_norm_dropout_softmax, self).__init__()

    def makeModel(self, bins=[100,100,100],feature_maps=[50], dim=[[3,3,3]] ,hidden_neurons=[300,300], dropout_rate=0.5,output_neurons=50) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

	

        main_input = Input(shape=(1,bins[0],bins[1],bins[2]), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        print(K.shape(main_input))
	convs=[main_input]
        for fm,dims in zip(feature_maps,dim):
            conv=Conv3D(fm,dims[0],dims[1],dims[2])(convs[-1])
            elu=ELU()(conv)
            convs.append(elu)
        #x = K.placeholder(n.float32, [None,1,1,1,1])
        #shape = K.function([x], K.shape(x))
        print(K.shape(convs[-1]))
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        #norm = BatchNormalization(name='norm')(main_input)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
        
        pooling = Flatten(name='pooling')(elu) #Average over time dimension
#        norm3 = BatchNormalization()(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [pooling]
        count=1
        for hidden in hidden_neurons:
            d_layer = ELU() (Dense(hidden,init="glorot_normal",name='hidden'+str(count))(hidden_layers[-1]))
            d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
 #               dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out = Dense(output_neurons,init="glorot_normal",name='out',activation='softmax')(hidden_layers[-1])
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='categorical_crossentropy') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target,output_neurons,bins):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target]
        return input_list,output_list
 


class Test_Conv_Network_dropout_ADAGrad_ELU_Activation_dont_mask_zeros_layerwise_batch_norm_dropout_softmax(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Conv_Network_dropout_ADAGrad_ELU_Activation_dont_mask_zeros_layerwise_batch_norm_dropout_softmax, self).__init__()

    def makeModel(self, bins=[100,100,100],feature_maps=[50], dim=[[3,3,3]] ,hidden_neurons=[300,300], dropout_rate=0.5,output_neurons=50) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

	

        main_input = Input(shape=(1,bins[0],bins[1],bins[2]), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        convs=[main_input]
        for fm,dims in zip(feature_maps,dim):
            conv=Conv3D(fm,dims[0],dims[1],dims[2])(convs[-1])
            elu=ELU()(conv)
            convs.append(elu)
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        #norm = BatchNormalization(name='norm')(main_input)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
        
        pooling = Flatten(name='pooling')(elu) #Average over time dimension

        pooling=Dropout(0.87,name="dropout_flatten")(pooling)
#        norm3 = BatchNormalization()(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [pooling]
        count=1
        for hidden in hidden_neurons:
            d_layer = ELU() (Dense(hidden,init="glorot_normal",name='hidden'+str(count))(hidden_layers[-1]))
            d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
 #               dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out = Dense(output_neurons,init="glorot_normal",name='out',activation='softmax')(hidden_layers[-1])
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='categorical_crossentropy') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target,output_neurons,bins):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target]
        return input_list,output_list
 


class Test_Conv_Network_max_pooling_ADAGrad_ELU_Activation_dont_mask_zeros_layerwise_batch_norm_dropout_softmax(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Conv_Network_max_pooling_ADAGrad_ELU_Activation_dont_mask_zeros_layerwise_batch_norm_dropout_softmax, self).__init__()

    def makeModel(self, bins=[100,100,100],feature_maps=[50], dim=[[3,3,3]] ,hidden_neurons=[300,300], dropout_rate=0.5,output_neurons=50,pool_grid=[2,2,2],pad_grid=[1,1,1],dropout_after_flatten=0.5):
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

	

        main_input = Input(shape=(1,bins[0],bins[1],bins[2]), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        convs=[main_input]
        out_dims=n.array(bins)
        for fm,dims in zip(feature_maps,dim):
            conv=Conv3D(fm,dims[0],dims[1],dims[2])(convs[-1])
            out_dims=out_dims-n.array(dims)+1
            print("out_dims: ",out_dims)
            elu=ELU()(conv)
            if not  ( n.all((out_dims % pool_grid)==0) ):
                 print("Padding with: ",pad_grid)
           	 pad=ZeroPadding3D((pad_grid[0],pad_grid[1],pad_grid[2]))(elu)
                 out_dims=out_dims+2*n.array(pad_grid)
           	 max_pool=MaxPooling3D((pool_grid[0],pool_grid[1],pool_grid[2]))(pad)
                 out_dims=out_dims // (n.array(pool_grid))
            else:
                 max_pool=MaxPooling3D((pool_grid[0],pool_grid[1],pool_grid[2]))(elu)
                 out_dims=out_dims // (n.array(pool_grid))
            convs.append(max_pool)

        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        #norm = BatchNormalization(name='norm')(main_input)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
        
        pooling = Flatten(name='pooling')(convs[-1]) #Average over time dimension
        if (dropout_after_flatten >0):
            print("Dropout after flatten",dropout_after_flatten)
            pooling=Dropout(dropout_after_flatten,name="dropout_after_flatten")(pooling)
#        norm3 = BatchNormalization()(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [pooling]
        count=1
        for hidden in hidden_neurons:
            d_layer = ELU() (Dense(hidden,init="glorot_normal",name='hidden'+str(count))(hidden_layers[-1]))
            d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
 #               dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out = Dense(output_neurons,init="glorot_normal",name='out',activation='softmax')(hidden_layers[-1])
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='categorical_crossentropy') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target,output_neurons,bins):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target]
        return input_list,output_list


class Test_Conv_Network_dropout_max_pooling_ADAGrad_ELU_Activation_dont_mask_zeros_layerwise_batch_norm_dropout_softmax(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Conv_Network_dropout_max_pooling_ADAGrad_ELU_Activation_dont_mask_zeros_layerwise_batch_norm_dropout_softmax, self).__init__()

    def makeModel(self, bins=[100,100,100],feature_maps=[50], dim=[[3,3,3]] ,hidden_neurons=[300,300], dropout_rate=0.5,output_neurons=50,pool_grid=[2,2,2],pad_grid=[1,1,1]) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

	

        main_input = Input(shape=(1,bins[0],bins[1],bins[2]), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        convs=[main_input]
        out_dims=n.array(bins)
        for fm,dims in zip(feature_maps,dim):
            conv=Conv3D(fm,dims[0],dims[1],dims[2])(convs[-1])
            out_dims=out_dims-n.array(dims)+1
            print("out_dims: ",out_dims)
            elu=ELU()(conv)
            if not  ( n.all((out_dims % pool_grid)==0) ):
                 print("Padding with: ",pad_grid)
           	 pad=ZeroPadding3D((pad_grid[0],pad_grid[1],pad_grid[2]))(elu)
                 out_dims=out_dims+2*n.array(pad_grid)
                 drop=Dropout(dropout_rate)(pad)
           	 max_pool=MaxPooling3D((pool_grid[0],pool_grid[1],pool_grid[2]))(drop)
                 out_dims=out_dims // (n.array(pool_grid))
            else:
                 drop=Dropout(dropout_rate)(elu)
                 max_pool=MaxPooling3D((pool_grid[0],pool_grid[1],pool_grid[2]))(drop)
                 out_dims=out_dims // (n.array(pool_grid))
            convs.append(max_pool)

        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        #norm = BatchNormalization(name='norm')(main_input)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
        
        pooling = Flatten(name='pooling')(convs[-1]) #Average over time dimension
#        norm3 = BatchNormalization()(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        hidden_layers =  [pooling]
        count=1
        for hidden in hidden_neurons:
            d_layer = ELU() (Dense(hidden,init="glorot_normal",name='hidden'+str(count))(hidden_layers[-1]))
            d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
 #               dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out = Dense(output_neurons,init="glorot_normal",name='out',activation='softmax')(hidden_layers[-1])
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='categorical_crossentropy') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target,output_neurons,bins):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = [features]
        output_list = [target]
        return input_list,output_list

class Test_Conv_Network_number_of_losses_ADAGrad_ELU_Activation_dont_mask_zeros_layerwise_batch_norm_dropout_softmax(NNWrapper):
    """
    Neural network using two dense hidden layers with ELU neurons, applying batch_norm to output of every layer
    """
    def __init__(self):
        super(Test_Conv_Network_number_of_losses_ADAGrad_ELU_Activation_dont_mask_zeros_layerwise_batch_norm_dropout_softmax, self).__init__()

    def makeModel(self, bins=[100,100,100],feature_maps=[50], dim=[[3,3,3]] ,hidden_neurons=[300,300], dropout_rate=0.5,output_neurons=50,sub_nn=[100,100]) :
        """
        Factory function for building the keras model

        Parameters:
        -----------
        e_losses_max: int
            Length of the time-dimension
        in_features: int
            Number of input features
        lstm_cells: int
            Number of LSTM cells
        hidden_neurons: list
            List of number of neurons per hidden layer
        dropout_rate: float
            Dropout rate applay to each layer
        """

	

        main_input = Input(shape=(1,bins[0],bins[1],bins[2]), dtype='float32', name='main_input') #in_features=4 for 3D x and 1D E
        convs=[main_input]
        for fm,dims in zip(feature_maps,dim):
            conv=Conv3D(fm,dims[0],dims[1],dims[2])(convs[-1])
            elu=ELU()(conv)
            convs.append(elu)
        #mask = Masking(0)(main_input) Maybe try out masking again? <- Try masking if 
        """
        Batch normalization should lead to better-behaved gradients
        Maybe test batch_norm after every layer? <-Try that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        #norm = BatchNormalization(name='norm')(main_input)
	'''
        lstm = LSTM(lstm_cells,
                    dropout_U=dropout_rate,
                    consume_less="gpu",
                    return_sequences=True)(norm)
        '''
        
        pooling = Flatten(name='pooling')(elu) #Average over time dimension
#        norm3 = BatchNormalization()(pooling)
#        auxiliary_loss = ELU()(Dense(1, name='aux_output')(norm3)) #Aux output after LSTM layer
        #Batch normalization at output????????? <- Probably not
        """
        Add the hidden layers with potential dropout
        """
        second_input = Input(shape=(1,), dtype='float32', name='second_input')
        sub_nn1 = ELU() (Dense(sub_nn[0],init="glorot_normal")(second_input))
        sub_nn2 = ELU() (Dense(sub_nn[1],init="glorot_normal")(sub_nn1))
        merge_layer = merge([pooling,sub_nn2], mode='concat', name='merge_layer')
        hidden_layers =  [merge_layer]
        count=1
        for hidden in hidden_neurons:
            d_layer = ELU() (Dense(hidden,init="glorot_normal",name='hidden'+str(count))(hidden_layers[-1]))
            d_layer = BatchNormalization(name='hidden_norm'+str(count))(d_layer)
            hidden_layers.append(d_layer)
            if dropout_rate > 0:
                dropout = Dropout(dropout_rate,name='dropout'+str(count))(hidden_layers[-1])
 #               dropout = BatchNormalization(name='dropout_norm'+str(count))(dropout)
                hidden_layers.append(dropout)
            count+=1
        out = Dense(output_neurons,init="glorot_normal",name='out',activation='softmax')(hidden_layers[-1])
        #Batch normalization at output???????????????????????????
        model = Model(input=[main_input,second_input], output=[out]) #Network yields mapped energies and aux loss
        model.compile(optimizer=Adagrad(), loss='categorical_crossentropy') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, num_losses, target, output_neurons):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs (out and aux loss), the model will need two targets
        """
        input_list = features
        losses = num_losses
        output_list = target
        return input_list,losses,output_list

