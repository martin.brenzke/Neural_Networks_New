#!/usr/bin/env zsh 
### Give the job a name, will be displayed in bjobs  default: Job_with_no_name
#BSUB -J test.job

### Max execution time in   hour:minutes   default: 00:15
#BSUB -W 6:00

### Max memory per core (RAM) in MB default: 512
### the product of numbers of cores and max memory has to be < 128000
#BSUB -M 6000

### number of cores [1-24] default: 1
#BSUB -n 1

# give a path for log and error file. %J is Job number %I is Job index
#BSUB -o /home/mb006358/test/%J.%I.out
#BSUB -e /home/mb006358/test/%J.%I.err

### these options are needed to run on our mashine
#BSUB -a gpu
#BSUB -R pascal
#BSUB -P phys3b

### try to do something on priority
### some int between 1 and 10, default is 5
### the higher the number the faster job starts
#BSUB -sp 1



### set up stuff
### load cuda
module load cuda

###Insert script running line like: python script.py --args arguments
python Test_job.py
