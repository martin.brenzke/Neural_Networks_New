import numpy as np
import configparser
import argparse
import io
import cPickle as cp
from matplotlib import pyplot as plt
import dashi as d

d.visual()

data=cp.load(open("data_set_350000.pickle",'rb'))

input1,targets=data["features"],data["target"]

#print(len(features))

xs=[]
ys=[]
zs=[]
x_pos=[]
y_pos=[]
z_pos=[]
energies=[]
e_pos=[]

masked_xs=[]
masked_ys=[]
masked_zs=[]
masked_energies=[]

masked_x_pos=[]
masked_y_pos=[]
masked_z_pos=[]
masked_e_pos=[]
count=0
indices=[]
for i in xrange(len(input1)):
	if not  (np.all(input1[i]==0)):
		#print("Event with all zeros found!!!!!!!!!!!!!!!!!")
		#print(i)
		indices.append(i)
		count+=1
        for j in xrange(len(input1[i])):
                xs.append(input1[i,j,0])
                ys.append(input1[i,j,1])
                zs.append(input1[i,j,2])
                energies.append(input1[i,j,3])
                e_pos.append(j)
                x_pos.append(j)
                y_pos.append(j)
                z_pos.append(j)
		if not (np.all(input1[i,j,:]==0)):
			masked_xs.append(input1[i,j,0])
			masked_ys.append(input1[i,j,1])
			masked_zs.append(input1[i,j,2])
			masked_energies.append(input1[i,j,3])
			masked_x_pos.append(j)
			masked_y_pos.append(j)
			masked_z_pos.append(j)
			masked_e_pos.append(j)

features_for_masking=input1[indices]
targets_for_masking=targets[indices]

features_and_targets={"features":features_for_masking,"target":targets_for_masking}

with open("data_set_350000_masking.pickle",'wb') as handle:
	cp.dump(features_and_targets,handle,protocol=2)

print("Number of all non-zero events: "+str(count))

#print(features_for_masking)

xs=np.array(xs)
ys=np.array(ys)
zs=np.array(zs)

x_pos=np.array(x_pos)
y_pos=np.array(y_pos)
z_pos=np.array(z_pos)

print("xs: "+str(xs))
print("x_pos: "+str(x_pos))


h_pos=d.factory.hist2d((xs,x_pos),50)
h_pos=h_pos.normalized()
h_pos.imshow()
cb=plt.colorbar()
cb.set_label("probability")
plt.xlabel("x position")
plt.ylabel("position in input")
plt.title("Positions of x in training input")
plt.tight_layout()
plt.savefig("x_positions_training_data.png")


plt.close()


h_pos=d.factory.hist2d((ys,y_pos),50)
h_pos=h_pos.normalized()
h_pos.imshow()
cb=plt.colorbar()
cb.set_label("probability")
plt.xlabel("y position")
plt.ylabel("position in input")
plt.title("Positions of y in training input")
plt.tight_layout()
plt.savefig("y_positions_training_data.png")


plt.close()



h_pos=d.factory.hist2d((zs,z_pos),50)
h_pos=h_pos.normalized()
h_pos.imshow()
cb=plt.colorbar()
cb.set_label("probability")
plt.xlabel("z position")
plt.ylabel("position in input")
plt.title("Positions of z in training input")
plt.tight_layout()
plt.savefig("z_positions_training_data.png")


plt.close()


h_pos=d.factory.hist2d((energies,e_pos),50)
h_pos=h_pos.normalized()
h_pos.imshow()
cb=plt.colorbar()
cb.set_label("probability")
plt.xlabel("energy loss")
plt.ylabel("position in input")
plt.title("Positions of energy losses in training input")
plt.tight_layout()
plt.savefig("energy_positions_training_data.png")


plt.close()
#####################################
#### Masked Stuff ###################
#####################################
h_pos=d.factory.hist2d((masked_xs,masked_x_pos),50)
h_pos=h_pos.normalized()
h_pos.imshow()
cb=plt.colorbar()
cb.set_label("probability")
plt.xlabel("x position")
plt.ylabel("position in input")
plt.title("Positions of x in training input (without zero entries)")
plt.tight_layout()
plt.savefig("x_positions_training_data_masked.png")


plt.close()


h_pos=d.factory.hist2d((masked_ys,masked_y_pos),50)
h_pos=h_pos.normalized()
h_pos.imshow()
cb=plt.colorbar()
cb.set_label("probability")
plt.xlabel("y position")
plt.ylabel("position in input")
plt.title("Positions of y in training input (without zero entries)")
plt.tight_layout()
plt.savefig("y_positions_training_data_masked.png")


plt.close()



h_pos=d.factory.hist2d((masked_zs,masked_z_pos),50)
h_pos=h_pos.normalized()
h_pos.imshow()
cb=plt.colorbar()
cb.set_label("probability")
plt.xlabel("z position")
plt.ylabel("position in input")
plt.title("Positions of z in training input (without zero entries)")
plt.tight_layout()
plt.savefig("z_positions_training_data_masked.png")


plt.close()


h_pos=d.factory.hist2d((masked_energies,masked_e_pos),50)
h_pos=h_pos.normalized()
h_pos.imshow()
cb=plt.colorbar()
cb.set_label("probability")
plt.xlabel("energy loss")
plt.ylabel("position in input")
plt.title("Positions of energy losses in training input (without zero entries)")
plt.tight_layout()
plt.savefig("energy_positions_training_data_masked.png")


plt.close()




####################################
##### Same stuff for shuffled data##
###################################
data=cp.load(open("Shuffled_data_350000.pickle",'rb'))

input1,targets=data["features"],data["target"]

#print(len(features))

xs=[]
ys=[]
zs=[]
x_pos=[]
y_pos=[]
z_pos=[]
energies=[]
e_pos=[]

for i in xrange(len(input1)):
        for j in xrange(len(input1[i])):
                xs.append(input1[i,j,0])
                ys.append(input1[i,j,1])
                zs.append(input1[i,j,2])
                energies.append(input1[i,j,3])
                e_pos.append(j)
                x_pos.append(j)
                y_pos.append(j)
                z_pos.append(j)

xs=np.array(xs)
ys=np.array(ys)
zs=np.array(zs)

x_pos=np.array(x_pos)
y_pos=np.array(y_pos)
z_pos=np.array(z_pos)

print("xs: "+str(xs))
print("x_pos: "+str(x_pos))


h_pos=d.factory.hist2d((xs,x_pos),50)
h_pos=h_pos.normalized()
h_pos.imshow()
cb=plt.colorbar()
cb.set_label("probability")
plt.xlabel("x position")
plt.ylabel("position in input")
plt.title("Positions of x in shuffled  input")
plt.tight_layout()
plt.savefig("x_positions_shuffled_training_data.png")


plt.close()


h_pos=d.factory.hist2d((ys,y_pos),50)
h_pos=h_pos.normalized()
h_pos.imshow()
cb=plt.colorbar()
cb.set_label("probability")
plt.xlabel("y position")
plt.ylabel("position in input")
plt.title("Positions of y shuffled  in input")
plt.tight_layout()
plt.savefig("y_positions_shuffled_training_data.png")


plt.close()



h_pos=d.factory.hist2d((zs,z_pos),50)
h_pos=h_pos.normalized()
h_pos.imshow()
cb=plt.colorbar()
cb.set_label("probability")
plt.xlabel("z position")
plt.ylabel("position in input")
plt.title("Positions of z in shuffled  input")
plt.tight_layout()
plt.savefig("z_positions_shuffled_training_data.png")


plt.close()


h_pos=d.factory.hist2d((energies,e_pos),50)
h_pos=h_pos.normalized()
h_pos.imshow()
cb=plt.colorbar()
cb.set_label("probability")
plt.xlabel("energy loss")
plt.ylabel("position in input")
plt.title("Positions of energy losses in shuffled  input")
plt.tight_layout()
plt.savefig("energy_positions_shuffled_training_data.png")


plt.close()

