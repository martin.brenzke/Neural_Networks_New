import numpy as np
import random as rng
import io
import cPickle as cp


data=cp.load(open("data_set_350000.pickle",'rb'))


features,targets=data["features"],data["target"]

#print("shape: "+str(features.shape))

rand=features.copy()

for i  in xrange(len(rand)):
	#print("features: "+str(features[i]))
	np.random.shuffle(rand[i])
	#print("rand: "+str(rand[i]))
	#raw_input("Press any key")
	#print(np.array_equal(features,rand))

shuffled_data={"features":rand,"target":targets}

with open("Shuffled_data_350000.pickle",'wb') as handle:
	cp.dump(shuffled_data,handle,protocol=2)
