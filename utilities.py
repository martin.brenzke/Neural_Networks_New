from keras.callbacks import Callback
import numpy as n
from keras import backend as K
from keras.objectives import binary_crossentropy, mean_squared_error

def customLoss(x, x_decoded):
    xent_loss = binary_crossentropy(x, x_decoded)
    mse_loss = mean_squared_error(x, x_decoded)
    return xent_loss + 2*mse_loss

class ReduceLROnPlateau(Callback):
    '''Reduce learning rate when a metric has stopped improving.
    Models often benefit from reducing the learning rate by a factor
    of 2-10 once learning stagnates. This callback monitors a
    quantity and if no improvement is seen for a 'patience' number
    of epochs, the learning rate is reduced.
    # Example
        ```python
            reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.2,
                                          patience=5, min_lr=0.001)
            model.fit(X_train, Y_train, callbacks=[reduce_lr])
        ```
    # Arguments
        monitor: quantity to be monitored.
        factor: factor by which the learning rate will
            be reduced. new_lr = lr * factor
        patience: number of epochs with no improvement
            after which learning rate will be reduced.
        verbose: int. 0: quiet, 1: update messages.
        mode: one of {auto, min, max}. In `min` mode,
            lr will be reduced when the quantity
            monitored has stopped decreasing; in `max`
            mode it will be reduced when the quantity
            monitored has stopped increasing; in `auto`
            mode, the direction is automatically inferred
            from the name of the monitored quantity.
        epsilon: threshold for measuring the new optimum,
            to only focus on significant changes.
        cooldown: number of epochs to wait before resuming
            normal operation after lr has been reduced.
        min_lr: lower bound on the learning rate.
    '''

    def __init__(self, monitor='val_loss', factor=0.1, patience=10,
                 verbose=0, mode='auto', epsilon=1e-4, cooldown=0, min_lr=0):
        super(Callback, self).__init__()

        self.monitor = monitor
        if factor >= 1.0:
            raise ValueError('ReduceLROnPlateau does not support a factor >= 1.0.')
        self.factor = factor
        self.min_lr = min_lr
        self.epsilon = epsilon
        self.patience = patience
        self.verbose = verbose
        self.cooldown = cooldown
        self.cooldown_counter = 0  # Cooldown counter.
        self.wait = 0
        self.best = 0
        self.mode = mode
        self.monitor_op = None
        self.reset()

    def reset(self):
        if self.mode not in ['auto', 'min', 'max']:
            warnings.warn('Learning Rate Plateau Reducing mode %s is unknown, '
                          'fallback to auto mode.' % (self.mode), RuntimeWarning)
            self.mode = 'auto'
        if self.mode == 'min' or (self.mode == 'auto' and 'acc' not in self.monitor):
            self.monitor_op = lambda a, b: n.less(a, b - self.epsilon)
            self.best = n.Inf
        else:
            self.monitor_op = lambda a, b: n.greater(a, b + self.epsilon)
            self.best = -n.Inf
        self.cooldown_counter = 0
        self.wait = 0
        self.lr_epsilon = self.min_lr * 1e-4

    def on_train_begin(self, logs={}):
        self.reset()

    def on_epoch_end(self, epoch, logs={}):
        logs['lr'] = K.get_value(self.model.optimizer.lr)
        current = logs.get(self.monitor)
        if current is None:
            warnings.warn('Learning Rate Plateau Reducing requires %s available!' %
                          self.monitor, RuntimeWarning)
        else:
            if self.cooldown_counter > 0:
                self.cooldown_counter -= 1
                self.wait = 0

            if self.monitor_op(current, self.best):
                self.best = current
                self.wait = 0
            elif self.cooldown_counter <= 0:
                if self.wait >= self.patience:
                    old_lr = float(K.get_value(self.model.optimizer.lr))
                    if old_lr > self.min_lr + self.lr_epsilon:
                        new_lr = old_lr * self.factor
                        new_lr = max(new_lr, self.min_lr)
                        K.set_value(self.model.optimizer.lr, new_lr)
                        if self.verbose > 0:
                            print('\nEpoch %05d: reducing learning rate to %s.' % (epoch, new_lr))
                        self.cooldown_counter = self.cooldown
                self.wait += 1

class FromSparseDataGenerator:
    def __init__(self, batch_size, data, normalize=True,
                 shuffle=True):
        self.__batch_size = batch_size
        self.__current_index = 0
        self.__data = data
        self.__data_len = data[0].shape[0]
        self.__normalize = normalize
        self.__shuffle = True

    def __call__(self):
        dataArr = [d for d in self.__data]
        new_epoch = True
        while True:
            if new_epoch and self.__shuffle:
                indices = n.random.choice(n.arange(self.__data[0].shape[0]),
                                      self.__data_len, replace=False)
                dataArr = [d[indices] for d in self.__data]
                new_epoch = False

            idx_low = self.__current_index
            if self.__current_index+self.__batch_size >= self.__data_len:
                idx_up = self.__data_len
                self.__current_index = 0
                new_epoch=True
            else:
                idx_up = self.__current_index+self.__batch_size
                self.__current_index = idx_up
            out = []
            for d in dataArr:
                try:
                    d_sliced = d[idx_low:idx_up,...].toarray()
                except AttributeError:
                    d_sliced = d[idx_low:idx_up,...]
                if self.__normalize:
                    d_sliced = d_sliced /d_sliced.max(axis=1)[:,n.newaxis]
                out.append(d_sliced)
            yield out 
