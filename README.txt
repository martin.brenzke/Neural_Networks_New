Scripts used for Deep Neural Networks for the Energy Reconstruction of Muon Events in IceCube.

Main scripts:
nn_models.py  -- contains all models
Train_Conv_Network.py -- training script for convolutional Neural Networks
Train_softmax_NN_no_fixed_random_seed.py  -- training script for fully connected Neural Networks resulting in a classification of events
