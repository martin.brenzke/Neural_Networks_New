import keras
from glob import glob
import cPickle as pickle
from keras import backend as K
K.set_image_dim_ordering('th')
from keras.layers import Input, Dense, Activation
from keras.models import Model
from keras.layers.core import Reshape, Flatten, Dropout
from keras.engine.topology import Merge
import importlib
from keras.layers.convolutional import Convolution3D, ZeroPadding3D
from keras.layers.pooling import MaxPooling1D, MaxPooling3D
from keras.optimizers import Nadam, Adagrad, Adadelta
from keras.layers.normalization import BatchNormalization
from keras.layers.advanced_activations import ELU
from keras.regularizers import activity_l1
from keras.layers.convolutional import UpSampling3D
import numpy as n
import scipy
from scipy import io
from utilities import ReduceLROnPlateau, FromSparseDataGenerator, customLoss
import argparse
import configparser


def makeModel_old(lr):
    inputs = Input(shape=(5160,))
    dense1 = Dense(90*60,init="glorot_normal", activation="relu")
    #encoded1 = dense1(BatchNormalization()(inputs))
    encoded1 = dense1(inputs)
    ahead = Dropout(0.5)(Dense(200, activation="relu")(encoded1))
    shaped = Reshape((1,9,10,60))(encoded1)
    convoluted = (Activation("relu")(Convolution3D(50,2,2,5,border_mode="same")(shaped)))
    #now shape is 50,9,10,60???????
    pooled = Dropout(0.5)(MaxPooling3D((2,2,5))(convoluted))
    #shape: (50,5,5,20)
    convoluted = (Activation("relu")(Convolution3D(100,2,2,3,border_mode="same")(pooled)))
    convoluted = (Activation("relu")(Convolution3D(100,2,2,3,border_mode="same")(convoluted)))
    convoluted = (Activation("sigmoid")(Convolution3D(100,2,2,3,border_mode="same")(convoluted)))
    convoluted = (Activation("sigmoid")(Convolution3D(100,2,2,3,border_mode="same")(convoluted)))
    encoded2 = Dropout(0.5)(Dense(500, activation="sigmoid")(Flatten()(pooled)))
    #pre_mul = Dropout(0.5)(Dense(100, activation="relu")(encoded2))
    merged = Merge(layers=None, mode="concat")([encoded2, ahead])
    out = Dense(1, activation="relu")(merged)
    
    model = Model(input=inputs, output=out)
    model.compile(optimizer="adadelta",
                  loss="MSE",
                  )
    return model 

parser = argparse.ArgumentParser()

parser.add_argument("-i",required=False, dest="input")
parser.add_argument("-r",required=False, dest="lr", default=0.001, type=float)
parser.add_argument("--configs",required=True,dest="config",nargs="+",help="Config File")
parser.add_argument("-o",required=True,dest="output")
parser.add_argument("-n",required=True,dest="nepochs")

args = parser.parse_args()

config=configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
for cfile in args.config:
    config.read(cfile)

train_cfg=config["training"]

if args.input:
    model = keras.models.load_model(args.input)
    model.optimizer.lr.set_value(args.lr)
else:
    model = makeModel(args.lr)
#Dateneinlesegedöns bearbeiten!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
feature_files = glob("pulses*features*.mtx") #edit path names
target_files = glob("pulses*target.npz") #edit path names

features = None
for f in feature_files:
    ff = io.mmread(f).tocsr()
    if features != None:
        features = scipy.sparse.vstack([features,ff])
    else:
        features = ff
target = None
for f in target_files:
    ff = n.log10(n.load(f)["target"])
    if target != None:
        target = n.concatenate([target, ff])
    else:
        target = ff
        
dataSize = features.shape[0]
batchSize =100
val_split=0.2
split_index = int(features.shape[0]*(1-val_split))
train_size = split_index
test_size = dataSize-train_size

dGen = FromSparseDataGenerator(batchSize,(features[:split_index,:], target[:split_index]),
                               normalize=False)
dGen_val =FromSparseDataGenerator(batchSize,(features[split_index:,:], target[split_index:]),
                               normalize=False)

#reduce_lr = ReduceLROnPlateau(monitor="loss", factor=0.2, patience=10, verbose=1, cooldown=3)
hist = model.fit_generator(dGen(),train_size,150,verbose=1, max_q_size=10,
                           validation_data=dGen_val(),
                           nb_val_samples=test_size)
pickle.dump(hist.history,open("pulseAutoEnc.pickle","w"))
model.save("pulseAutoEnc.hdf")
try:
    encoder.save("pulseEnc.hdf")
    decoder.save("pulseDec.hdf")
except:
    print "Implement me :("
