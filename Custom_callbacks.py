import keras
import numpy as np
import io
import cPickle as cp 
class normalized_cross_correlation(keras.callbacks.Callback):

	def __init__(self, f):
		self._first_term = []
		self._normalized_cross_correlations = []
		self._nominator = []
		self._initial_l2 = []
		self._new_l2 = []
		self.filepath = f
	def on_train_begin(self, logs={}):
		initial_weights = np.array(self.model.get_weights())
		initial_means = []
		for i in xrange(len(initial_weights)):
			if (len(initial_weights[i].shape) > 2):
				ax = tuple([k for k in xrange(1,len(initial_weights[i].shape))])
				initial_means.append(np.sum(initial_weights[i], axis=ax)/(initial_weights[i].size))
				self._initial_l2.append(np.sqrt(np.sum(initial_weights[i]**2, axis=ax)))
			else:
				initial_means.append(np.sum(initial_weights[i], axis=0)/(initial_weights[i].shape[0]))
				self._initial_l2.append(np.sqrt(np.sum(initial_weights[i]**2, axis=0)))
		initial_means = np.array(initial_means)
		for o in xrange(len(initial_means)):
			self._first_term.append(initial_weights[o] - initial_means[o])
		self._fisrt_term = np.array(self._first_term)
		self._initial_l2 = np.array(self._initial_l2) 
		return

	def on_train_end(self, logs={}):
		with open(self.filepath,'w') as handle:
			cp.dump(self._normalized_cross_correlations, handle) 
		return
	'''
	def on_epoch_begin(self, logs={}):
		return
	'''



	def on_epoch_end(self, logs={}):
		new_weights = np.array(self.model.get_weights())
		second_term = []
		new_means = []	
		for j in xrange(len(new_weights)):
			if (len(new_weights[j].shape) > 2):
				ax = tuple([k for k in xrange(1,len(new_weights[j].shape))])
				new_means.append(np.sum(new_weights[i], axis=ax) / (new_weights[i].size))
				#second_term = new_weights - new_means
				#self._nominator.append(np.sum(self._first_term[j]*second_term), axis=ax)
				self._new_l2.append(np.sqrt(np.sum(new_weights[j]**2, axis=ax)))
			else:
				new_means.append(np.sum(new_weights[i], axis=0) / (new_weights[i].shape[0]))
				#second_term = new_weights - new_means
				#self._nominator.append(np.sum(self._first_term[j]*second_term), axis=0)
				self._new_l2.append(np.sqrt(np.sum(new_weights[j]**2,axis=0)))
		new_means = np.array(new_means)
		for l in xrange(len(new_means)):
			second_term.append(new_weights[l] - new_means[l])
		second_term = np.array(second_term)
		for p in xrange(len(new_weights)):
			if (len(new_weights[p].shape) > 2):
				ax = tuple([k for k in xrange(1,len(new_weights[p].shape))])
				self._nominator.append(np.sum(self._first_term[p]*second_term[p], axis=ax))
			else:
				self._nominator.append(np.sum(self._first_term[p]*second_term[p],axis=0))		

		self._new_l2 = np.array(self._new_l2)
		self._nominator = np.array(self._nominator)
		self._normalized_cross_correlations.append(self._nominator/(self._initial_l2*self._new_l2))
		return


	'''
	def on_batch_begin(self, logs={}):
		return

	def on_batch_end(self, logs={}):
		return
	'''
