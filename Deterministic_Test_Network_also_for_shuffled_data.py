[model]
class=Test_Mini_Network_ADAGrad

[model_settings]
lstm_cells=300
hidden_neurons=[1050,50]
e_losses_max=169
in_features=4
dropout_rate=${training:dropout_rate}

[training]
learning_rate=0.01
validation_split=0.15
dropout_rate=0.
lr_scaling=0
sample_size=350000
batch_size=300
patience=300
