import keras

ver = keras.__version__[0]

print("Running with version:", ver)

if (ver == '1'):
	from Custom_callbacks_2 import normalized_cross_correlation
	print("Imported callbacks for version 1")
if (ver == '2'):
	from Custom_callbacks_2_keras_2 import normalized_cross_correlation
	print("Imported callbacks for version 2")
