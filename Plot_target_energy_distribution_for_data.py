import numpy as np
import cPickle as cp
from matplotlib import pyplot as plt

file=np.load("data_set_300000.pickle",mmap_mode='r')

targets=file["target"]

data=targets[:178500]

print(len(data))

plt.hist(targets,100)
plt.xlabel(r"log$_{10}(\frac{E_{target}}{GeV})$")
plt.ylabel("Count")
plt.title("Distribution of target energies in training data")
plt.tight_layout()
plt.savefig("Target_energy_distribution_first_178500.png")
