import numpy as np
import argparse
import configparser
import io
import os

parser = argparse.ArgumentParser()
parser.add_argument("-o", dest="outprefix", help="NN output prefix", required=True)
parser.add_argument("-n", dest="nepochs", help="Number of epochs", required=True)
parser.add_argument("--configs", dest="configs", help="Config files", nargs="+", required=True)
parser.add_argument("-s",dest="shuffle",help="Turn shuffling on/off (1/0)",required=True)
args = parser.parse_args()
arguments = " -o $(OUTFILE) -n {} --configs {} -s {}".format(args.nepochs,
                                                       " ".join(args.configs),
                                                       args.shuffle
                                                               )
args=parser.parse_args()

config=configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
for cfile in args.configs:
    config.read(cfile)
model = config["model"]["class"]

print("model:", model)
print("outprefix:", args.outprefix)
print(args.configs)
fullmodelname = os.path.splitext(args.configs[0])[0]
modelname = fullmodelname.split("/")[-1]
print("modelname:", modelname)

with io.FileIO("Current_model.txt", 'w') as file:
	file.write(model+"_settings.txt")

with io.FileIO(model+"_settings.txt", 'w') as handle:
	handle.write(args.nepochs+"\n"+args.shuffle+"\n")
	for u in xrange(len(args.configs)):
		handle.write(args.configs[u]+"\n")
	handle.write(args.outprefix+modelname+"\n")

