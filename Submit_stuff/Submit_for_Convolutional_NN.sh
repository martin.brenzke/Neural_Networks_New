#!/usr/bin/env zsh

current_model=Current_model.txt

filename=(`cat "$current_model"`)
declare -a things
things=(`cat "$filename"`)
echo ${things[@]}

echo ${things[1]}
nepochs=${things[1]}
sh=${things[2]}
conf=${things[3]}
out=${things[4]}

 
### Give the job a name, will be displayed in bjobs  default: Job_with_no_name
#BSUB -J NN.job

### Max execution time in   hour:minutes   default: 00:15
#BSUB -W 48:00

### Max memory per core (RAM) in MB default: 512
### the product of numbers of cores and max memory has to be < 128000
#BSUB -M 6000

### number of cores [1-24] default: 1
#BSUB -n 1

# give a path for log and error file. %J is Job number %I is Job index
#BSUB -o out.out
#BSUB -e out.err

### these options are needed to run on our mashine
#BSUB -a gpu
#BSUB -R pascal
#BSUB -P phys3b

### try to do something on priority
### some int between 1 and 10, default is 5
### the higher the number the faster job starts
#BSUB -sp 5



### set up stuff
### load cuda
module load cuda/80

###Insert script running line like: python script.py --args arguments
python ../Train_Conv_Network.py -n $nepochs -s $sh --configs $conf -o $out
