import keras
from keras.models import Model, Sequential
from keras.layers.core import Dense, Activation, TimeDistributedDense, Masking, Dropout, Flatten, Reshape
from keras.layers.convolutional import Convolution2D
from keras.layers import Input, merge
from keras.layers.pooling import GlobalAveragePooling1D
from keras.layers.recurrent import LSTM
from keras.layers.wrappers import TimeDistributed
from keras.layers.normalization import BatchNormalization
from keras.optimizers import Nadam, SGD, Adagrad
from keras.callbacks import LearningRateScheduler
from keras.layers.advanced_activations import ELU
from keras import backend as K
from keras.layers.core import Masking
import numpy as n
import argparse


class NNWrapper(object):
    """
    Abstract base class for neural networks
    """
    def __init__(self):
        self.__model = None

    def get_model(self):
        """
        Getter for model
        """
        if not self.__model:
            self.makeModel()
        return self.__model

    def set_model(self, model):
        """
        Setter for model
        """
        self.__model = model

    model = property(get_model, set_model)

    def loadModel(self, filename):
        self.model = keras.models.load_model(filename)

    def makeModel(self):
        raise NotImplementedError("Needs to be implemented for derived class")

    def prepareData(self):
        raise NotImplementedError("Needs to be implemented for derived class")

    def saveModel(self, filename):
        self.model.save(filename)

class Simple_CNN(NNWrapper):
    """
    Recurrent neural network using a LSTM layer and multiple dense
    hidden layers with ELU neurons
    """
    def __init__(self):
        super(Simple_CNN, self).__init__()

    def makeModel(self, e_losses_max, in_features,
                  lstm_cells=300, hidden_neurons=[300,300], dropout_rate=0.5) :
        
        main_input=Input(shape=(5160,),name='main_input')
        reshape=Reshape((1,86,60,1))(main_input)
        conv1=Convolution2D(50,5,5,activation='tanh')(reshape) #yields shape (1,82,56,50) if kernel is only applied to valid positions
        pool1=AveragePooling2D()(conv1) #standard arguments lead to halving the image size in each dimension, returns (1,41,28,50)
#        conv2=Convolution2D(50,3,3,activation='tanh')(pool1) #yields shape (1,80,54,50)
#        pool2=AveragePooling2D()(conv2)
        flat=Flatten()(pool1)
        out=Dense(1,activation='relu')(flat)
        model = Model(input=[main_input], output=[out])
        model.compile(optimizer=Nadam(), loss='mean_squared_error') #Maybe try RMSProp?

        self.model = model

    def prepareData(self, features, target):
        """
        Prepare the training and testing data lists for model.fit()

        Since we have two outputs, the model will need two targets
        """
        input_list = [features]
        output_list = [target, target]

        return input_list, output_list

