import numpy as np
from matplotlib import pyplot as plt
from numpy.random import RandomState
from sklearn import model_selection as cv
import dashi as d

d.visual()

bins=[10,10,10]

data=np.load("/data/user/mbrenzke/NN/JungleParty/branches/martins_tests/python/neural_networks/Data_for_CNN_grid_"+str(bins[0])+"_"+str(bins[1])+"_"+str(bins[2])+".npz")

features,target = data["arr_0"],data["arr_1"]
rng = RandomState(0)
features_train,features_test,target_train,target_test = cv.train_test_split(features,target,train_size=.99,random_state=rng)
bins=np.array([2.+i*(7.-2.)/50. for i in xrange(50)])
print(bins)

validation_split=0.15
val_len=len(target_train)*validation_split
val_trues = target_train[len(target_train)-val_len:]
print(val_trues[:3])
E_trues=[]
for j in xrange(len(val_trues)):
	index=np.where(val_trues[j])[0]
	E_trues.append(bins[index])
E_trues=np.array(E_trues)
print(E_trues[:3])  

h=d.factory.hist1d(E_trues,bins)
h.line()
plt.xlabel(r"log($\frac{E_{true}}{GeV}$)")
plt.ylabel("count")
plt.title("True energeis of validation events for CNN")
plt.savefig("True_energies_for_validatin_events_for_CNN.png")

