import numpy as np
import configparser
import argparse
import io
import cPickle as cp

parser=argparse.ArgumentParser()
parser.add_argument("--configs",required=True,dest="configs",help="config file")
args=parser.parse_args()



config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read(args.configs)
print(config.keys())
train_cfg=config["training"]
sample_size=eval(train_cfg["sample_size"])
full_data=np.load("/data/user/chaack/data/muongun/MuonGunLosses.npz",mmap_mode='r')
reduced_features=full_data["features"][0:sample_size]
reduced_targets=full_data["target"][0:sample_size]
features_and_targets={"features":reduced_features,"target":reduced_targets}

with open("data_set_"+str(sample_size)+".pickle",'wb') as handle:
        #np.save(file=handle,arr=features_and_targets,allow_pickle=False)
	cp.dump(features_and_targets,handle,protocol=2)
#cp.dump(features_and_targets,open("data_set_"+str(sample_size)+".pickle"))

#with io.FileIO("data_set_"+str(sample_size),'w') as file:
#	file.write(str(features_and_targets)+".txt")



