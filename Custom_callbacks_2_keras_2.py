import keras
import numpy as np
import io
import cPickle as cp
 
class normalized_cross_correlation(keras.callbacks.Callback):

	def __init__(self, f):
		self._first_term = []
		self._normalized_cross_correlations = {"epoch":[], "layer":[], "value":[]}
		#self._nominator = []
		self._initial_l2 = []
		#self._new_l2 = []
		self.filepath = f

	def on_train_begin(self, logs={}):
		initial_weights = np.array(self.model.get_weights())
		layers = self.model.layers
		batch = "BatchNormalization"
		for u in xrange(len(layers)):
			if (layers[u].__class__.__name__ == batch):
				continue
			initial_weights = layers[u].get_weights()
			for i in xrange(len(initial_weights)):
				if(len(initial_weights[i].shape) > 2):
					ax = tuple([k for k in xrange(0,len(initial_weights[i].shapei)-1)])
					size = np.prod(initial_weights[i].shape[:-1])
					means_per_feature_map = np.sum(initial_weights[i], axis=ax)/size   #(initial_weights[i].size)
					print("means_per_feature_map.shape", means_per_feature_map.shape)
					print("initial_weights[i].shape", initial_weights[i][0].shape)
					tmp_first_term = []
					for m in xrange(len(means_per_feature_map)):
						tmp_first_term.append(initial_weights[i][m] - means_per_feature_map[m])
					self._first_term.append(tmp_first_term)
					self._initial_l2.append(np.sqrt(np.sum(initial_weights[i]**2, axis=ax)))
				else:
					size = initial_weights[i].shape[0]
					mean = np.sum(initial_weights[i], axis=0)/size    #(initial_weights[i].size)
					self._first_term.append(initial_weights[i] - mean)
					self._initial_l2.append(np.sqrt(np.sum(initial_weights[i]**2, axis=0)))
		self._first_term = np.array(self._first_term)
		self._initial_l2 = np.array(self._initial_l2)
		print("shape first_term[0]:", np.array(self._first_term[0]).shape)
		return

	def on_train_end(self, logs={}):
		with open(self.filepath,'w') as handle:
			cp.dump(self._normalized_cross_correlations, handle) 
		return
	'''
	def on_epoch_begin(self, logs={}):
		return
	'''



	def on_epoch_end(self, epoch, logs={}):
		#new_weights = np.array(self.model.get_weights())
		#second_term = []
		#new_l2 = []
		layers = self.model.layers
		batch = "BatchNormalization"
		layer_count = 0

		for u in xrange(len(layers)):
			if (layers[u].__class__.__name__ == batch):
				continue
			if not (layers[u].get_weights()):
				continue
			new_weights = layers[u].get_weights()
			for p in xrange(len(new_weights)):
				if (len(new_weights[p].shape) > 2):
					second_term = []
					ax = tuple([l for l in xrange(0,len(new_weights[p].shape)-1)])
					size = np.prod(new_weights[p].shape[:-1])
					means_per_feature_map = np.sum(new_weights[p], axis=ax)/size    #(new_weights[p].size)
					for g in xrange(len(means_per_feature_map)):
						second_term.append(new_weights[p][g] - means_per_feature_map[g] )
					new_l2 = np.sqrt(np.sum(new_weights[p]**2, axis=ax))
					second_term = np.array(second_term)
					nominator = np.sum(self._first_term[p+2*layer_count] * second_term, axis=ax)
					self._normalized_cross_correlations["epoch"].append(epoch)
					self._normalized_cross_correlations["value"].append(nominator/(new_l2*self._initial_l2[p+2*layer_count]                                                                                           ))
					self._normalized_cross_correlations["layer"].append(layers[u].name)		
				else:
					size = new_weights[p].shape[0]
					mean = np.sum(new_weights[p], axis=0)/size     #(new_weights[p].size)
					second_term = new_weights[p] - mean
					new_l2 = np.sqrt(np.sum(new_weights[p]**2, axis=0))
					nominator = np.sum(self._first_term[p+2*layer_count] *second_term, axis=0)
					self._normalized_cross_correlations["epoch"].append(epoch)
					self._normalized_cross_correlations["value"].append(nominator/(new_l2*self._initial_l2[p+2*layer_count]                                                                                           ))
					self._normalized_cross_correlations["layer"].append(layers[u].name)
			layer_count += 1
	#	second_term = np.array(second_term)
	#	new_l2 = np.array(new_l2)
	#	print()
	#	nominator = np.sum(self._first_term * second_term, axis=ax)
	#	self._normalized_cross_correlations.append(nominator/(new_l2*self._initial_l2))
		return


	'''
	def on_batch_begin(self, batch,logs={}):
		return

	def on_batch_end(self, batch,logs={}):
		return
	'''
