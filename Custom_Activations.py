import keras
from keras.layers import Layer
from keras import backend as K


class SELU(Layer):
    '''Scaled Exponential Linear Unit.

    It follows:
    `f(x) = lamb * alpha * (exp(x) - 1.) for x <= 0`,
    `f(x) = lamb * x for x > 0`.
    # Input shape
        Arbitrary
    # Output shape
        Same shape as the input
    #Arguments
        alpha: scale for the negative factor,
        lamb: scale for both factors.
    # References
        -[Self-Normalizing Neural Networks](https://arxiv.org/abs/1706.02515)
    '''

    def __init__(self, alpha=1.6733, lamb=1.0507, **kwargs):
        super(SELU, self).__init__(**kwargs)
        self.supports_masking = True
        self.alpha = K.cast_to_floatx(alpha)
        self.lamb = K.cast_to_floatx(lamb)

    def call(self, inputs):
        return K.switch(inputs > 0, self.lamb * inputs, self.lamb * self.alpha * (K.exp(inputs) - 1.))

    def get_config(self):
        config = {'alpha': float(self.alpha), 'lamb': float(self.lamb)}
        base_config = super(SELU, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))
